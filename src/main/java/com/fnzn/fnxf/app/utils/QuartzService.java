package com.fnzn.fnxf.app.utils;

import com.fnzn.fnxf.app.mapper.consumer.ConsumerCommendMapper;
import com.fnzn.fnxf.app.mapper.consumer.StaffReportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class QuartzService {
    @Autowired
    private StaffReportMapper staffReportMapper;
    @Autowired
    private ConsumerCommendMapper consumerCommendMapper;
    //    每天凌晨0点启动
    @Scheduled(cron = "0 0 0 * * ?")
    public void timerToNow(){
        boolean one = staffReportMapper.updateInvalidOnThreeDay(3);
        boolean two = consumerCommendMapper.updateInvalidOnThreeDay(3);
        boolean three = staffReportMapper.updateFollowOnDay(30);
        boolean four = consumerCommendMapper.updateFollowOnDay(30);
    }

}