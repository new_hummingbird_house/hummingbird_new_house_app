package com.fnzn.fnxf.app.utils;

import java.security.MessageDigest;
import java.util.Base64;

/**
 * @author Administrator
 */
public class ConstantsUtils {

    /**
     * 加密算法
     * @param sData 传输数据
     * @param sKey 密钥
     * @return
     */
    public static String signData(String sData, String sKey) {
        byte[] md = new byte[0];
        try {
            byte[] btInput = (sData+sKey).getBytes("UTF-8");
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("SHA-256");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            md = mdInst.digest();
        } catch (Exception e) {
            //TODO: do something
            return null;
        }
        return new String(Base64.getUrlEncoder().encode(md));
    }


}
