package com.fnzn.fnxf.app.utils;

/**
 * @author Administrator
 */
public class Constants {

    /**
     * 短信签名信息
     */
    public final static String SIGN_NAME = "蜂鸟智能";
    /**
     * accessKeyId
     */
    public final static String ACCESS_KEY_ID = "LTAICCTG6pSZ6MBL";
    /**
     * accessKeySecret
     */
    public final static String ACCESS_KEY_SECRET = "LtX8QEIy7InsMew5GhpEOSRjEj6mHx";
    /**
     * 短信模板参数
     */
    public static final String CODE_CAPTCHA_VAR_NAME = "code";
    /**
     * 用户注册默认图片
     */
    public static final String CUSTOMERPIC = "/customer/headImage/customerDefaultPicture.jpg";
    /**
     * 登录短信验证码模板code
     */
    public static String CAPTCHA_TEL_CODE_LOGIN = "SMS_139860235";
    /**
     * 购买信息确认短信验证码模板code
     */
    public static String CAPTCHA_TEL_CODE_CONFIRM = "SMS_141580271";
    /**
     * 置业顾问客户提醒短信模板code
     */
    public static String FOLLOW_TEL_CODE_REMIND = "SMS_141605421";
    /**
     * 删除标记
     */
    public final static String DEL_FLAG = "del_flag";
    /**
     * 删除
     */
    public final static String STATUS_DEL = "1";
    /**
     * 正常
     */
    public final static String STATUS_NORMAL = "0";

    /**
     * 审核状态(0:待审核；1:审核通过；2:审核未通过)
     */
    public final static String AUDIT_STAY = "0";
    public final static String AUDIT_PASS = "1";
    public final static String AUDIT_FAIL = "2";

    /**
     * 代金券类型(0：固定商品；1：优惠券)
     */
    public final static String VOUCHER_MALL = "0";
    public final static String VOUCHER_COUPON = "1";

    /**
     * 代金券状态(0：未使用；1：已使用；2：已过期)
     */
    public final static String STATUS_UNUSED = "0";
    public final static String STATUS_USED = "1";
    public final static String STATUS_EXPIRED = "2";

    /**
     * 代金券截止日期(三个月)
     */
    public final static int DUETIME = 3;

    /**
     * ecnodeRules规则
     */
    public static final String ENCODERULES = "12345";

    /**
     * 代金券二维码保存路径
     */
    public static final String QRPATH = "/fnxf/image/qrCode/";
}
