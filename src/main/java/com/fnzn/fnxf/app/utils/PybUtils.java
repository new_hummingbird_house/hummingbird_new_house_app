package com.fnzn.fnxf.app.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;


@Component
public class PybUtils {
    @Value("${upload.wPath}")
    private String wPath;
    @Value("${upload.path}")
    private String lPath;
    private static final String DEFAULT_IMAGE_TYPE = ".jpg";
    public static String MD5Twich(String password) {
        if (objectIsNotEmpty(password)) {
            String one = MD5Utils.MD5Encode(password, "UTF-8");
            String two = MD5Utils.MD5Encode(one, "UTF-8");
            return two;
        } else {
            return null;
        }
    }

    public static boolean objectIsEmpty(Object obj) {
        if (obj == null || "".equals(obj)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean objectIsNotEmpty(Object obj) {
        if (obj != null && !"".equals(obj)) {
            return true;
        } else {
            return false;
        }
    }

    public static <T> List<T> jsonToListObj(String json, Class<?> elementClass) {
        List<T> list = new ArrayList<>();
        if (objectIsEmpty(json)) {
            return null;
        } else {
            ObjectMapper mapper = new ObjectMapper();
            CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, elementClass);
            SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(myDateFormat);
            try {
                list = mapper.readValue(json, listType);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    public String uploadFile(byte[] file,String filePath,String fileType) throws Exception {
        String fileName = UUID.randomUUID()
                                .toString()
                                .replace("-","");
        File targetFile = new File(filePath);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        String mainPath = "";
        if(PybUtils.isWindows()){
            mainPath = wPath;
        }else{
            mainPath = lPath;
        }
        //相对路径
        String relativePath = filePath+fileName+fileType;
        //绝对路径
        String absolutePath = mainPath+filePath+fileName+fileType;
        FileOutputStream out = new FileOutputStream(absolutePath);
        out.write(file);
        out.flush();
        out.close();
        return relativePath;
    }

    public String moveFile(String oldPath,String newPath){
        return "";
    }

    public  boolean delFile(String fileName){
        String mainPath = "";
        if(PybUtils.isWindows()){
            mainPath = wPath;
        }else{
            mainPath = lPath;
        }
        File file = new File(mainPath+fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

    public static boolean isWindows(){
        Properties props=System.getProperties(); //获得系统属性集
        String osName = props.getProperty("os.name"); //操作系统名称
        if(osName.toLowerCase().contains("windows")){
            return true;
        }else{
            return false;
        }
    }

    //存储base64文件到服务器
    public String GenerateImage(String imgStr,String filePath)
    {   //对字节数组字符串进行Base64解码并生成图片
        if (objectIsEmpty(imgStr)) //图像数据为空
            return "";
        imgStr = imgStr.substring(imgStr.indexOf(",")+1,imgStr.length());
        BASE64Decoder decoder = new BASE64Decoder();
        OutputStream out = null;
        try
        {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for(int i=0;i<b.length;++i)
            {
                if(b[i]<0)
                {//调整异常数据
                    b[i]+=256;
                }
            }
            String mainPath = "";
            if(PybUtils.isWindows()){
                mainPath = wPath;
            }else{
                mainPath = lPath;
            }
            String fileName = UUID.randomUUID()
                    .toString()
                    .replace("-","");
            //相对路径
            String relativePath = filePath+fileName+DEFAULT_IMAGE_TYPE;
            //绝对路径
            String absolutePath = mainPath+filePath+fileName+DEFAULT_IMAGE_TYPE;
            System.out.println("rel:"+relativePath);
            System.out.println("abo:"+absolutePath);
            out = new FileOutputStream(absolutePath);
            out.write(b);
            out.flush();
            out.close();
            return relativePath;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }finally {
            try {
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(isWindows());
    }
}
