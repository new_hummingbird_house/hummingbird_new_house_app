package com.fnzn.fnxf.app.web.core;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.entity.core.CoreDict;
import com.fnzn.fnxf.app.mapper.core.CoreDictMapper;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.spring.web.json.Json;

import java.util.List;

@RestController
@RequestMapping(value = "/coreDict")
public class CoreDictController {

    @Autowired
    private CoreDictMapper coreDictMapper;

    @GetMapping("/getDictByType")
    public JsonResult getCoreDict(@RequestParam(name = "dictName",required = true) String dictName){
        List<CoreDict> coreDicts = coreDictMapper.selectList(
                new EntityWrapper<CoreDict>()
                        .eq("TYPE", dictName)
                        .eq("del_flag","0")
        );
        return JsonResult.success(coreDicts);
    }
}
