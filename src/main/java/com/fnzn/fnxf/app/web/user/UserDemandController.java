package com.fnzn.fnxf.app.web.user;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.user.UserDemand;
import com.fnzn.fnxf.app.service.user.UserDemandService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pyb
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/userDemand")
public class UserDemandController {

    @Autowired
    private UserDemandService userDemandService;

    @GetMapping(value = "/findUserDemand")
    public JsonResult findUserDemandByUserId(){
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        UserDemand userDemand = userDemandService.selectUserDemand(customer.getId());
        return JsonResult.success(userDemand);
    }

    @GetMapping(value = "/findUserDemandDict")
    public JsonResult findUserDemandDict(){
        Map<String, Object> userDemandDict = userDemandService.userDemandDict();
        return JsonResult.success(userDemandDict);
    }

    @PostMapping(value = "/saveOrUpdateDemand")
    public JsonResult saveOrUpdate(UserDemand userDemand){
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        if(userDemand!=null){
            userDemand.setUserId(customer.getId());
            boolean isSuccess = userDemandService.insertOrUpdate(userDemand);
            return JsonResult.success(isSuccess);
        }else{
            return JsonResult.failMessage("数据均为空");
        }
    }

}
