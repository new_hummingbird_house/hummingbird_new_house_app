package com.fnzn.fnxf.app.web.voucher;

import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.service.voucher.IVoucherInfoService;
import com.fnzn.fnxf.app.utils.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代金券信息表 前端控制器
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@RestController
@RequestMapping("/voucherInfo")
@ResponseBody
public class VoucherInfoController {

    @Autowired
    private IVoucherInfoService voucherInfoService;

    @RequestMapping(value = "/voucherQuery", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "代金券信息查询")
    public JsonResult voucherQuery(@RequestParam(value = "status") String status){

        List<VoucherInfoVO> voucherInfoVOList = voucherInfoService.selectVoucherInfoByStatus(status);

        return JsonResult.success(voucherInfoVOList);
    }

    @RequestMapping(value = "/voucherDetailQuery", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "代金券详细信息")
    public JsonResult voucherDetailQuery(@RequestParam(value = "id") String id){

        VoucherInfoVO voucherInfoVO = voucherInfoService.selectVoucherDetailById(id);

        return JsonResult.success(voucherInfoVO);
    }

    @RequestMapping(value = "/voucherUse", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "代金券")
    public JsonResult voucherUseForBusiness(@RequestParam(value = "content") String content){

        Map<String,Object> map = voucherInfoService.voucherUseForBusiness(content);
        String msg = (String) map.get("msg");
        if(!msg.equals("")){
            return JsonResult.failMessage(msg);
        }else{
            VoucherInfoVO voucherInfoVO = (VoucherInfoVO) map.get( "VO");
            return JsonResult.success(voucherInfoVO);
        }
    }

    @RequestMapping(value = "/voucherCheckPass", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "代金券")
    public JsonResult voucherCheckPass(@RequestParam(value = "id") String id){
        JsonResult jsonResult = new JsonResult();

        Map<String,Object> map = voucherInfoService.voucherCheckPass(id);
        String msg = (String) map.get("msg");
        String code = (String) map.get("code");
        jsonResult.setMsg(msg);
        jsonResult.setCode(code);
        return jsonResult;
    }

    @RequestMapping(value = "/getVoucherInfo", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "代金券")
    public JsonResult getVoucherInfo(@RequestParam(value = "code") String qrCode){
        JsonResult jsonResult = new JsonResult();

        Map<String,Object> map = voucherInfoService.getVoucherInfo(qrCode);
        String msg = (String) map.get("msg");
        String code = (String) map.get("code");
        String data = (String) map.get("data");
        jsonResult.setMsg(msg);
        jsonResult.setCode(code);
        jsonResult.setData(data);
        return jsonResult;
    }

}
