package com.fnzn.fnxf.app.web.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fnzn.fnxf.app.bean.consumer.ConsumerComVO;
import com.fnzn.fnxf.app.bean.consumer.ReportDictVO;
import com.fnzn.fnxf.app.bean.consumer.StaffReportVO;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.building.BuildingAdviserMapper;
import com.fnzn.fnxf.app.service.consumer.EmployeeFollowService;
import com.fnzn.fnxf.app.service.consumer.StaffReportService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.PybUtils;
import com.fnzn.fnxf.app.utils.SendSmsUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 员工报备
 */

@RestController
@RequestMapping("/staff")
public class StaffReportController {

    @Autowired
    private StaffReportService staffReportService;
    @Autowired
    private BuildingAdviserMapper adviserMapper;
    @Autowired
    private EmployeeFollowService employeeFollowService;

    @GetMapping(value = "/report")
    public JsonResult staffReportQuery(){
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        List<StaffReportVO> staffReportVOList = staffReportService.findStaffReportVO(customer.getId());
        return JsonResult.success(staffReportVOList);
    }

    @PostMapping(value = "/report")
    public JsonResult staffReportInsert(StaffReport staffReport) throws JsonProcessingException {
        if(staffReport!=null){
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            staffReport.setCustomerId(customer.getId());
            /**标志为报备记录**/
            staffReport.setType(1);
            BuildingAdviser buildingAdviser = new BuildingAdviser();
            buildingAdviser.setPhone(customer.getLoginId());
            Integer adviserId = adviserMapper.selectOne(buildingAdviser).getId();
            if(PybUtils.objectIsEmpty(adviserId)){
                return JsonResult.failMessage("报备者不是置业顾问");
            }
            staffReport.setAdviserId(adviserId);
            boolean sortSuccess = staffReportService.adviserRestartSort(staffReport.getAdviserId(),
                    staffReport.getBuildingInfoId());
            boolean isSame = employeeFollowService.isSamePhone(staffReport.getPhone(),
                    staffReport.getSparePhone(),
                    staffReport.getBuildingInfoId());
            if(isSame){
                return JsonResult.failMessage("该手机号已被报备过");
            }
            boolean isSuccess = staffReportService.insert(staffReport);
            if(isSuccess){
                sendMsg(staffReport);
                return JsonResult.success("报备成功");
            }else{
                return JsonResult.failMessage("报备失败");
            }
        }else{
            return JsonResult.failMessage("填写数据为空");
        }
    }

    @GetMapping("/reportDict")
    public JsonResult findReportDict(){
        ReportDictVO reportDict = staffReportService.findReportDict();
        return JsonResult.success(reportDict);
    }

    @GetMapping("/adviserByBId")
    public JsonResult findAdviserByBuildingId(Integer buildingId){
        Map<String,Object> map = new HashMap<>();
        if(PybUtils.objectIsEmpty(buildingId)){
            return JsonResult.success(map);
        }else{
            map = staffReportService.findAdviserOneByBuildingId(buildingId);
        }
        return JsonResult.success(map);
    }

    @GetMapping(value = "/findAllAdviser")
    public JsonResult findAllAdviser(){
        List<BuildingAdviser> advisers = adviserMapper.selectList(
                new EntityWrapper<BuildingAdviser>()
        );
        return JsonResult.success(advisers);
    }

    @GetMapping(value = "/adviserByKd")
    public JsonResult findAdviserByKeyword(String keyword){
        if(PybUtils.objectIsEmpty(keyword)) {
            return JsonResult.failMessage("关键字为空");
        }
        List<BuildingAdviser> advisers = adviserMapper.selectList(
                new EntityWrapper<BuildingAdviser>().like("name", keyword)
        );
        return JsonResult.success(advisers);
    }

    /***--------推荐数据分割线------**/
    /**
     * 推荐人查询推荐人记录
     * @return
     */
    @GetMapping(value = "/commend")
    public JsonResult consumerCommendQuery(){
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        List<StaffReportVO> staffReportVOList = staffReportService.findStaffReportVO(customer.getId());
        return JsonResult.success(staffReportVOList);
    }

    /**
     * 推荐人保存报备记录
     * @param staffReport
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping(value = "/commend")
    public JsonResult consumerCommendInsert(StaffReport staffReport) throws JsonProcessingException {
        if(staffReport!=null){
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            staffReport.setCustomerId(customer.getId());
            /**标志为推荐记录**/
            staffReport.setType(2);
            BuildingAdviser buildingAdviser = new BuildingAdviser();
            buildingAdviser.setPhone(customer.getLoginId());
            /**
             * 顾问ID问题
             */
            boolean sortSuccess = staffReportService.adviserRestartSort(staffReport.getAdviserId(),
                    staffReport.getBuildingInfoId());
            boolean isSame = employeeFollowService.isSamePhone(staffReport.getPhone(),
                    staffReport.getSparePhone(),
                    staffReport.getBuildingInfoId());
            if(isSame){
                return JsonResult.failMessage("该手机号已被报备过");
            }
            boolean isSuccess = staffReportService.insert(staffReport);
            if(isSuccess){
                sendMsg(staffReport);
                return JsonResult.success("报备成功");
            }else{
                return JsonResult.failMessage("报备失败");
            }
        }else{
            return JsonResult.failMessage("填写数据为空");
        }
    }

    /***
     * 向所有的顾问成员发送短信
     * @param staffReport
     */
    public void sendMsg(StaffReport staffReport){
        List<BuildingAdviser> advisers = adviserMapper.selectList(
                new EntityWrapper<BuildingAdviser>().in("id", staffReport.getMemberIds())
        );
        if(advisers!=null&&advisers.size()>0){
            for(BuildingAdviser adviser : advisers){
                String code = Constants.FOLLOW_TEL_CODE_REMIND;
                Map map = new TreeMap();
                map.put("name",staffReport.getRealName());
                map.put("phone",staffReport.getPhone());
                ObjectMapper objectMapper = new ObjectMapper();
                String json = null;
                try {
                    json = objectMapper.writeValueAsString(map);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                String s = SendSmsUtil.sendSms(code, adviser.getPhone(), json);
            }
        }

    }
}
