package com.fnzn.fnxf.app.web.consumer;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.logs.OperateLog;
import com.fnzn.fnxf.app.service.consumer.EmployeeFollowService;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/employee")
public class EmployeeFollowController {
    @Autowired
    private EmployeeFollowService employeeFollowService;

    @GetMapping(value = "/follow")
    public JsonResult findMyCustomer(Integer sourceType,Integer id){
        Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
        if(PybUtils.objectIsNotEmpty(sourceType)&&PybUtils.objectIsNotEmpty(id)){
            Map<String, Object> myCustomer = employeeFollowService.getMyCustomerById(sourceType,id ,customer.getLoginId());
            return JsonResult.success(myCustomer);
        }else{
            List<Map<String, Object>> customerRecord = employeeFollowService.findMyCustomer(customer.getLoginId());
            return JsonResult.success(customerRecord);
        }
    }

    @PutMapping(value = "/progress")
    public JsonResult updateProgressStatus(OperateLog operateLog,
                                           @RequestParam(value = "sourceType",required = true) Integer sourceType,
                                           @RequestParam(value = "sourceId",required = true) Integer sourceId){
        Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
        boolean isSuccess = false;
        if(operateLog!=null&&operateLog.getUpdateStatus()!=null){
            isSuccess = employeeFollowService.updateProgressStatus(operateLog, sourceType, sourceId, customer.getLoginId());
        }
        if(isSuccess){
            return JsonResult.successMessage("修改成功");
        }else{
            return JsonResult.failMessage("修改失败");
        }
    }
}
