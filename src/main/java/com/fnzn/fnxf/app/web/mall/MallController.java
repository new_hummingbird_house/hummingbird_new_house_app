package com.fnzn.fnxf.app.web.mall;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.bean.mall.MallInfoListVO;
import com.fnzn.fnxf.app.bean.mall.MallInfoVO;
import com.fnzn.fnxf.app.bean.mall.MallProductBuyVO;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.mapper.mall.MallInfoMapper;
import com.fnzn.fnxf.app.service.mall.MallInfoService;
import com.fnzn.fnxf.app.service.mall.MallInfoVOService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.QRCodeFactory;
import com.fnzn.fnxf.app.utils.SymmetricEncoder;
import com.google.zxing.WriterException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author Administrator
 */
@Api
@RestController
@RequestMapping(value = "/mall")
public class MallController {

    @Autowired
    private MallInfoService mallInfoService;

    @Autowired
    private MallInfoVOService mallInfoVOService;

     /**
     * @return
     */
    @RequestMapping(value = "/getMallList", method = RequestMethod.GET)
    @ApiOperation(value = "商城列表")
    private JsonResult getMallList(@RequestParam(value="type") String type,@RequestParam(value="keyWord") String search,@RequestParam(value="sys") String sys) {

        MallInfoListVO mallInfoListVO = mallInfoService.selectMallInfoList(type,search);

        return JsonResult.success(mallInfoListVO);
    }

    /**
     * @return
     */
    @RequestMapping(value = "/getMallDetail", method = RequestMethod.GET)
    @ApiOperation(value = "商品详情")
    private JsonResult getMallDetail(@RequestParam(value="id") String id) {

        MallInfoVO MallInfoVO = mallInfoVOService.selectMallInfoDetailById(id);

        return JsonResult.success(MallInfoVO);
    }


    /**
     * @return
     */
    @RequestMapping(value = "/buyMallProduct", method = RequestMethod.GET)
    @ApiOperation(value = "商品购买")
    private JsonResult buyMallProduct(@RequestParam(value="id") String id) {

        MallProductBuyVO mallProductBuyVO = mallInfoService.selectMallProductById(id);

        return JsonResult.success(mallProductBuyVO);
    }

    @Autowired
    MallInfoMapper mallInfoMapper;

    /**
     * @return
     */
    @RequestMapping(value = "/getQrCode", method = RequestMethod.GET)
    @ApiOperation(value = "生成商品二维码")
    private JsonResult getQrCode() throws IOException, WriterException {

        String returnCode = mallInfoService.gerQrCode();
        if("000".equals(returnCode)){
            return JsonResult.success();
        }else{
            return JsonResult.fail();
        }
    }
}
