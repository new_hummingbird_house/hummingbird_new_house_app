package com.fnzn.fnxf.app.web.building;

import com.fnzn.fnxf.app.bean.building.BuildingInfoAndApartmentVO;
import com.fnzn.fnxf.app.service.building.BuildingInfoService;
import com.fnzn.fnxf.app.utils.JsonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api
@Controller
public class AppIndexController {

    @Autowired
    private BuildingInfoService buildingInfoService;

    @GetMapping(value = "/noLogin")
    @ResponseBody
    public JsonResult noLogin(){
        return JsonResult.failMessage("用户没登陆");
    }

    @GetMapping(value = "/noAuth")
    @ResponseBody
    public JsonResult noAuth(){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode("403");
        jsonResult.setMsg("该用户没权限");
        return jsonResult;
    }

    @GetMapping(value = "/")
    public String indexHtml(){
        return "views/index/index.html";
    }

    @GetMapping(value = "/index.json")
    @ResponseBody
    public JsonResult<List<BuildingInfoAndApartmentVO>> index(){
        Map map = new HashMap<>();
        map.put("rand",true);
        List<BuildingInfoAndApartmentVO> buildingInfoAndApartmentVOS = buildingInfoService.selectInfoAndApart(map);
        return JsonResult.success(buildingInfoAndApartmentVOS);
    }
}
