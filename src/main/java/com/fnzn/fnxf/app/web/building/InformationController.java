package com.fnzn.fnxf.app.web.building;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fnzn.fnxf.app.entity.building.Information;
import com.fnzn.fnxf.app.service.building.InformationService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/zixun")
public class InformationController {
    /**资讯service**/
    @Autowired
    private InformationService informationService;

    @GetMapping(value = "/index")
    @ResponseBody
    public JsonResult index(){
        List<Information> informationList = informationService.selectPage(
                new Page<Information>(1,10),
                new EntityWrapper<Information>().eq("del_flag",0))
                .getRecords();
        return JsonResult.success(informationList);
    }

    @GetMapping(value = "/details")
    @ResponseBody
    public JsonResult details(Integer id){
        Information information = informationService.selectAndUpdateReadNum(id);
        return JsonResult.success(information);
    }
}
