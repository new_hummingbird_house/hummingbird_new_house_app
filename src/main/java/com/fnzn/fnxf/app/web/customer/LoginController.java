package com.fnzn.fnxf.app.web.customer;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.service.customer.LoginService;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.google.zxing.WriterException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@Api
@RestController
@RequestMapping(value = "/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/byPassword", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "用户登录--登录方式：手机号、密码")
    @ResponseBody
    public JsonResult loginfor_pw(@RequestParam(value = "loginId") String loginId, @RequestParam(value = "password")String password){
        Map<String,Object> map = loginService.loginforps(loginId,password);

        return getJsonResult(map);
    }

    @RequestMapping(value = "/byCode", method = { RequestMethod.POST ,RequestMethod.GET })
    @ApiOperation(value = "用户登录--登录方式：手机号、验证码")
    @ResponseBody
    public JsonResult<String> loginfor_code(String loginId, HttpServletRequest request) throws IOException, WriterException {
        Map<String,Object> map = loginService.loginforcode(loginId);

        return getJsonResult(map);

    }

    /**
     * 获取手机验证码
     * @param loginId
     * @param type
     * @return
     */
    @RequestMapping(value = "/getCode", method = RequestMethod.GET)
    @ApiOperation(value = "用户登录--获取手机验证码")
    public JsonResult<String> getCode(String loginId, String type) {

        //登录凭证不能为空
        if (loginId == null || "".equals(loginId)) {
            return JsonResult.failMessage("手机号不能为空！");
        }
        String returnCode = loginService.getCode(loginId,type);

        if(returnCode!=null&&!"".equals(returnCode)){
            return JsonResult.success(returnCode);
        }else{
            return JsonResult.failMessage("验证码获取失败！");
        }

    }

    private JsonResult getJsonResult(Map<String, Object> map) {
        if(map==null||map.isEmpty()){
            return JsonResult.failMessage("登录失败！");
        }
        int return_code = (int)map.get("code");
        if(return_code == 0){
            Customer customer = (Customer) map.get("customer");

            UsernamePasswordToken token = new UsernamePasswordToken(customer.getLoginId(),
                    String.valueOf(customer.isFirst()),true);
            SecurityUtils.getSubject().login(token);
            return JsonResult.success(map);
        }else{
            String msg = (String) map.get("msg");
            return JsonResult.failMessage(msg);
        }
    }

}
