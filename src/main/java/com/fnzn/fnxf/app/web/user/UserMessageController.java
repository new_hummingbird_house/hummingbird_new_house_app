package com.fnzn.fnxf.app.web.user;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.service.user.UserMessageService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 记录用户短信 前端控制器
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@RestController
@RequestMapping("/userMessage")
public class UserMessageController {

    @Autowired
    private UserMessageService userMessageService;

    @PostMapping("/record")
    public JsonResult recordUserMessage(String message, HttpServletRequest request){
        String ipAddress = request.getRemoteAddr();
        Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
        userMessageService.recordUserMessage(message,customer.getId());
        return JsonResult.success();
    }
}
