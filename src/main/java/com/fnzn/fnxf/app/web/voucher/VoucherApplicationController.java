package com.fnzn.fnxf.app.web.voucher;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.bean.voucher.VoucherVO;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.voucher.VoucherApplication;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;
import com.fnzn.fnxf.app.mapper.building.BuildingAdviserMapper;
import com.fnzn.fnxf.app.mapper.building.BuildingInfoMapper;
import com.fnzn.fnxf.app.mapper.voucher.VoucherInfoMapper;
import com.fnzn.fnxf.app.service.mall.MallInfoService;
import com.fnzn.fnxf.app.service.voucher.IVoucherApplicationService;
import com.fnzn.fnxf.app.service.voucher.IVoucherInfoService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.QRCodeFactory;
import com.fnzn.fnxf.app.utils.SymmetricEncoder;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 代金券申请表 前端控制器
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@RestController
@RequestMapping("/voucherApplication")
public class VoucherApplicationController {

    @Autowired
    private IVoucherApplicationService voucherApplicationService;
    @Autowired
    private IVoucherInfoService voucherInfoService;
    @Autowired
    private BuildingInfoMapper buildingInfoMapper;
    @Autowired
    private BuildingAdviserMapper buildingAdviserMapper;
    @Autowired
    private MallInfoService mallInfoService;
    @Autowired
    private VoucherInfoMapper voucherInfoMapperer;

    /**
     * 获取用户信息和所有顾问
     *
     * @param buildingInfoId
     * @return
     */
    @RequestMapping(value = "/getVoucherVO", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户信息和所有顾问")
    @ResponseBody
    public JsonResult getVoucherVO(@RequestParam Integer buildingInfoId) {
        try {
            VoucherVO voucherVO = new VoucherVO();
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            if (customer != null) {
                BuildingInfo buildingInfo = buildingInfoMapper.selectById(buildingInfoId);
                voucherVO.setBuildingTitle(buildingInfo.getBuildingTitle());
                List<BuildingAdviser> buildingAdvisers = buildingAdviserMapper.selectAdviserListByBuildingId(buildingInfoId);
                voucherVO.setBuildingAdviserList(buildingAdvisers);
                voucherVO.setRealName(customer.getRealName());
                voucherVO.setPhone(customer.getLoginId());
                return JsonResult.success(voucherVO);
            }
            return JsonResult.failMessage("该用户没有登陆");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.failMessage(e.getMessage());
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "新增代金券申请")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public JsonResult add(VoucherApplication voucherApplication) {
        if (voucherApplication != null) {
            voucherApplication.setSubmissionTime(new Date());
            voucherApplication.setStatus(Constants.AUDIT_STAY);
            boolean isSuccess = voucherApplicationService.insert(voucherApplication);
            if (isSuccess) {
                return JsonResult.success("申请提交成功");
            } else {
                return JsonResult.failMessage("提交失败");
            }
        } else {
            return JsonResult.failMessage("填写数据为空");
        }
    }

    /**
     * 获取代金券待审核申请
     *
     * @return
     */
    @RequestMapping(value = "/getVoucherApplicationToCheck", method = RequestMethod.GET)
    @ApiOperation(value = "获取代金券待审核申请")
    @ResponseBody
    public JsonResult getVoucherApplicationToCheck() {
        try {
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            if (customer != null) {
                String loginId = customer.getLoginId();
                List<VoucherApplication> list = voucherApplicationService.getVoucherApplicationToCheck(Constants.AUDIT_STAY, loginId);
                return JsonResult.success(list);
            }
            return JsonResult.failMessage("该用户没有登陆");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.failMessage(e.getMessage());
        }
    }

    /**
     * 获取代金券已审核申请
     *
     * @return
     */
    @RequestMapping(value = "/getVoucherApplicationChecked", method = RequestMethod.GET)
    @ApiOperation(value = "获取代金券已审核申请")
    @ResponseBody
    public JsonResult getVoucherApplicationChecked() {
        try {
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            if (customer != null) {
                String loginId = customer.getLoginId();
                List<VoucherApplication> list = voucherApplicationService.getVoucherApplicationChecked(Constants.AUDIT_PASS, Constants.AUDIT_FAIL, loginId);
                return JsonResult.success(list);
            }
            return JsonResult.failMessage("该用户没有登陆");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.failMessage(e.getMessage());
        }
    }

    /**
     * 通过ID获取代金券申请
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    @ApiOperation(value = "通过ID获取代金券申请")
    @ResponseBody
    public JsonResult getById(Integer id) {
        try {
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            if (customer != null) {
                EntityWrapper entityWrapper = new EntityWrapper();
                entityWrapper.eq("id", id);
                entityWrapper.setSqlSelect("id, name, phone, identity_card AS identityCard, building_info_id AS buildingInfoId, (select CONCAT(f.product_name,f.product_model) from mall_info f where f.id = voucher_application.var1) as productName, (select building_title from building_info where id = building_info_id ) as buildingTitle, apartment, adviser_id AS adviserId, submission_time AS submissionTime, check_time AS checkTime, status, create_time AS createTime, update_time AS updateTime, del_flag AS delFlag, var1, var2");
                VoucherApplication voucherApplication = voucherApplicationService.selectOne(entityWrapper);
                return JsonResult.success(voucherApplication);
            }
            return JsonResult.failMessage("该用户没有登陆");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.failMessage(e.getMessage());
        }
    }

    /**
     * 审核提交
     *
     * @param voucherApplication
     * @return
     */
    @RequestMapping(value = "/editByCheck", method = RequestMethod.GET)
    @ApiOperation(value = "审核提交")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public JsonResult editByCheck(VoucherApplication voucherApplication) {
        try {
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            if (customer != null) {
                voucherApplication.setCheckTime(new Date());
                voucherApplication.setUpdateTime(new Date());
                boolean isSuccess = voucherApplicationService.updateById(voucherApplication);
                if (isSuccess) {
                    String fileName = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
                    String format = "jpg";
                    String outFileUrl = Constants.QRPATH + fileName + "." + format;
                    voucherApplication = voucherApplicationService.selectById(voucherApplication.getId());
                    VoucherInfo voucherInfo = new VoucherInfo();
                    voucherInfo.setVoucherApplicationId(voucherApplication.getId());
                    voucherInfo.setLoginId(voucherApplication.getPhone());
                    voucherInfo.setType(Constants.VOUCHER_MALL);
                    voucherInfo.setMallInfoId(Integer.valueOf(voucherApplication.getVar1()));
                    MallInfo mallInfo = mallInfoService.selectById(Integer.valueOf(voucherApplication.getVar1()));
                    voucherInfo.setVoucherName("兑换券：省" + mallInfo.getProductSalePrice() + "元");
                    voucherInfo.setPreferential(mallInfo.getProductSalePrice());
                    voucherInfo.setStartTime(new Date());
                    voucherInfo.setDueTime(getAfterThreeMonthsDate());
                    voucherInfo.setStatus(Constants.STATUS_UNUSED);
                    voucherInfo.setQrPath("/qrCode/" + fileName + "." + format);
                    voucherInfo.setCreateTime(new Date());
                    voucherInfo.setDelFlag(Constants.STATUS_NORMAL);
                    voucherInfo.setQrCode(fileName);
                    voucherInfoMapperer.add(voucherInfo);

                    Integer voucherInfoId = voucherInfo.getId();
                    String content = String.valueOf(voucherInfoId) + ";" + customer.getLoginId();
                    content = SymmetricEncoder.AESEncode(Constants.ENCODERULES, content);
                    QRCodeFactory qrCodeFactory = new QRCodeFactory();
                    qrCodeFactory.CreatQrImage(content, format, outFileUrl, null, null);
                    return JsonResult.success("审核成功");
                } else {
                    return JsonResult.failMessage("审核失败");
                }
            }
            return JsonResult.failMessage("该用户没有登陆");
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.failMessage(e.getMessage());
        }
    }

    /**
     * 计算截止日期(三月后)
     *
     * @return
     */
    public Date getAfterThreeMonthsDate() {
        Date nowDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);
        //截止日期 三个月
        calendar.add(Calendar.MONTH, Constants.DUETIME);
        Date afterDate = calendar.getTime();
        return afterDate;
    }
}
