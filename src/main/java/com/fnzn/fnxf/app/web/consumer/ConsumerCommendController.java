package com.fnzn.fnxf.app.web.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fnzn.fnxf.app.bean.consumer.ConsumerComVO;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.building.BuildingAdviserMapper;
import com.fnzn.fnxf.app.service.consumer.ConsumerCommendService;
import com.fnzn.fnxf.app.service.consumer.EmployeeFollowService;
import com.fnzn.fnxf.app.service.consumer.StaffReportService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.SendSmsUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 用户推荐
 */

@RestController
@RequestMapping("/consumer")
public class ConsumerCommendController {

    @Autowired
    private ConsumerCommendService consumerCommendService;
    @Autowired
    private StaffReportService staffReportService;
    @Autowired
    private BuildingAdviserMapper adviserMapper;
    @Autowired
    private EmployeeFollowService employeeFollowService;

    @GetMapping(value = "/commend")
    public JsonResult consumerCommendQuery(){
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        List<ConsumerComVO> consumerComVOList = consumerCommendService.findConsumerComVO(customer.getId());
        return JsonResult.success(consumerComVOList);
    }

    @PostMapping(value = "/commend")
    public JsonResult staffReportInsert(ConsumerCommend consumerCommend) throws JsonProcessingException {
        if(consumerCommend!=null){
            Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
            consumerCommend.setCustomerId(customer.getId());
            boolean sortSuccess = staffReportService.adviserRestartSort(consumerCommend.getAdviserId()
                    ,consumerCommend.getBuildingInfoId());
            boolean isSame = employeeFollowService.isSamePhone(consumerCommend.getPhone(),
                    consumerCommend.getSparePhone(),
                    consumerCommend.getBuildingInfoId());
            if(isSame){
                return JsonResult.failMessage("该手机号已被报备过");
            }
            boolean isSuccess = consumerCommendService.insert(consumerCommend);
            if(isSuccess){
                String phone = adviserMapper.selectById(consumerCommend.getAdviserId()).getPhone();
                String code = Constants.FOLLOW_TEL_CODE_REMIND;
                Map map = new TreeMap();
                map.put("name",consumerCommend.getRealName());
                map.put("phone",consumerCommend.getPhone());
                map.put("building_info_id",consumerCommend.getBuildingInfoId());
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(map);
                String s = SendSmsUtil.sendSms(code, phone, json);
                return JsonResult.success("推荐成功");
            }else{
                return JsonResult.failMessage("推荐失败");
            }
        }else{
            return JsonResult.failMessage("填写数据为空");
        }
    }
}
