package com.fnzn.fnxf.app.web.browsingHistory;

import com.fnzn.fnxf.app.entity.browsingHistory.BrowsingHistory;
import com.fnzn.fnxf.app.service.browsingHistory.IBrowsingHistoryService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 浏览信息控制表 前端控制器
 * </p>
 *
 * @author xiaomh
 * @since 2018-09-03
 */
@RestController
@RequestMapping("/browsingHistory")
public class BrowsingHistoryController {

    @Autowired
    private IBrowsingHistoryService browsingHistoryService;

    @RequestMapping("/check")
    private JsonResult checkBrowsingHistory(String id){
        List<BrowsingHistory> browsingHistoryList = browsingHistoryService.checkBrowsingHistory(id);
        return JsonResult.success(browsingHistoryList);

    }

}
