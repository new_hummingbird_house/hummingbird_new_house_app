package com.fnzn.fnxf.app.web.user;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.service.user.UserContactsService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户通讯录记录表 前端控制器
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@RestController
@RequestMapping("/userContacts")
public class UserContactsController {

    @Autowired
    private UserContactsService userContactsService;

    @PostMapping("/record")
    public JsonResult recordUserContacts(String contacts, HttpServletRequest request){
        String ipAddress = request.getRemoteAddr();
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        userContactsService.recordUserContacts(contacts,ipAddress,customer.getId());
        return JsonResult.success();
    }
}
