package com.fnzn.fnxf.app.web.customer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.bean.core.RolePermissionVO;
import com.fnzn.fnxf.app.entity.adviser.BuildingInfoAdviser;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.adviser.BuildingInfoAdviserMapper;
import com.fnzn.fnxf.app.mapper.customer.CustomerMapper;
import com.fnzn.fnxf.app.service.customer.CustomerService;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.PybUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

@Api
@RestController
@RequestMapping(value = "/wode")
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerMapper customerMapper;
    private BuildingInfoAdviserMapper buildingInfoAdviserMapper;

    @GetMapping(value = "/demo")
    public String demo(HttpServletRequest request, HttpServletResponse response){
        Customer customer = customerService.selectById("1");
        UsernamePasswordToken token = new UsernamePasswordToken(customer.getLoginId(),
                customer.getPassword(),true);
        SecurityUtils.getSubject().login(token);
        SavedRequest savedRequest= WebUtils.getSavedRequest(request);
        if(null!=savedRequest){
            return "redirect:" + savedRequest.getRequestUrl();
        }
        return null;
    }

    @GetMapping(value = "/quitLogin")
    @ApiOperation(value = "退出登录系统")
    public JsonResult customerExit(HttpSession session){
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return JsonResult.success();
    }

    @GetMapping(value = "/customer")
    @ApiOperation(value = "查询用户信息接口")
    public JsonResult<RolePermissionVO> customerQuery(){
        Subject subject = SecurityUtils.getSubject();
        Customer customer = (Customer)subject.getPrincipal();
        if(customer!=null){
            RolePermissionVO customerRolAndPer = customerService.findCustomerRolAndPer(customer);
            return JsonResult.success(customerRolAndPer);
        }else{
            return JsonResult.failMessage("该用户没有登陆");
        }
    }

    @PutMapping(value = "/customer")
    @ApiOperation(value = "更新用户信息接口")
    public JsonResult customerUpdate(Customer newCustomer,Integer buildingInfoId,String oldPassword,String newPassword){
        Customer loginCustomer = (Customer)SecurityUtils.getSubject().getPrincipal();
        Customer oldCustomer = customerService.selectById(loginCustomer.getId());
        //没有设置过密码
        if(oldCustomer.getPassword()==null){
            newPassword = PybUtils.MD5Twich(newPassword);
            if(PybUtils.objectIsNotEmpty(newPassword)){
                //设置新密码
                newCustomer.setPassword(newPassword);
            }
        }
        //已经设置过密码
        else{
            oldPassword = PybUtils.MD5Twich(oldPassword);
            if(oldPassword!=null){
                if(oldPassword.equals(oldCustomer.getPassword())){
                    newPassword = PybUtils.MD5Twich(newPassword);
                    if(PybUtils.objectIsNotEmpty(newPassword)){
                        //设置新密码
                        newCustomer.setPassword(newPassword);
                    }
                }else{
                    return JsonResult.failMessage("原密码输入错误");
                }
            }
        }
        if(loginCustomer!=null){
            newCustomer.setId(loginCustomer.getId());
            newCustomer.setType(null);
            boolean isSuccess = customerService.updateCustomerInfo(newCustomer,buildingInfoId);
            if(isSuccess){
                return JsonResult.success();
            }else{
                return  JsonResult.failMessage("更新发生异常");
            }
        }else{
            return JsonResult.failMessage("该用户没有登陆");
        }
    }


}
