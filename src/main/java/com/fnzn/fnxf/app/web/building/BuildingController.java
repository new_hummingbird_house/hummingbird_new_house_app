package com.fnzn.fnxf.app.web.building;

import com.fnzn.fnxf.app.bean.building.BuildingInfoAndRoleVO;
import com.fnzn.fnxf.app.entity.building.BuildingApartment;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.bean.building.BuildingInfoDetailsVO;
import com.fnzn.fnxf.app.bean.building.BuildingInfoAndApartmentVO;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.service.building.BuildingApartmentService;
import com.fnzn.fnxf.app.service.building.BuildingInfoService;
import com.fnzn.fnxf.app.service.building.CustomerTelService;
import com.fnzn.fnxf.app.utils.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/building")
public class BuildingController {
    @Autowired
    private BuildingInfoService buildingInfoService;

    @Autowired
    private BuildingApartmentService buildingApartmentService;

    @Autowired
    private CustomerTelService customerTelService;

    @GetMapping(value = "/list.json")
    public JsonResult buildingList(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        List<BuildingInfoAndApartmentVO> list = buildingInfoService.selectInfoAndApart(parameterMap);
        return JsonResult.success(list);
    }


    @RequestMapping(value = "/details", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "楼盘详情")
    @ResponseBody
    public JsonResult buildingDetails(@RequestParam(value = "id") String id){

        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        BuildingInfoDetailsVO buildingInfoDetailsVO = buildingInfoService.buildingDetails(id,customer);

        buildingInfoDetailsVO.setCustomer(customer);

        return JsonResult.success(buildingInfoDetailsVO);
    }

    @RequestMapping(value = "/apartment", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "楼盘户型")
    @ResponseBody
    public JsonResult buildingApartment(@RequestParam(value = "id") String id){
        System.out.println(id);
        List<BuildingApartment> buildingInfoDetails = buildingApartmentService.selectApartmentByBuildingId(id);

        return JsonResult.success(buildingInfoDetails);
    }

    @RequestMapping(value = "/detailsMore", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "楼盘详情--查看更多")
    @ResponseBody
    public JsonResult buildingInfoMore(@RequestParam(value = "id") String id){

        BuildingInfo buildingInfo = buildingInfoService.selectBuildingInfoMore(id);

        return JsonResult.success(buildingInfo);
    }


    @RequestMapping(value = "/telHistory", method = { RequestMethod.POST,RequestMethod.GET })
    @ApiOperation(value = "楼盘详情--记录用户拔打电话历史记录")
    @ResponseBody
    public JsonResult telHistory(@RequestParam(value = "phone") String phone){

        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        customerTelService.insertTelHistory(phone,customer);

        return JsonResult.success();
    }

    @GetMapping(value = "/buildAndRole")
    public JsonResult roleAndBuild(){
        BuildingInfoAndRoleVO buildingInfoAndRoleVO = buildingInfoService.selectBuildAndRole();
        if(buildingInfoAndRoleVO==null){
            return JsonResult.failMessage("用户没登陆");
        }else{
            return JsonResult.success(buildingInfoAndRoleVO);
        }
    }
}
