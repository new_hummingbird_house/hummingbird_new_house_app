package com.fnzn.fnxf.app.web.logs;

import com.fnzn.fnxf.app.service.logs.OperateLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 员工操作日志 前端控制器
 * </p>
 *
 * @author xiaomh
 * @since 2018-08-03
 */
@RestController
@RequestMapping("/operateLog")
public class OperateLogController {

    @Autowired
    private OperateLogService operateLogService;

}
