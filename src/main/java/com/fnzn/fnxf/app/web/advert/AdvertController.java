package com.fnzn.fnxf.app.web.advert;

import com.fnzn.fnxf.app.entity.advert.Advert;
import com.fnzn.fnxf.app.service.advert.AdvertService;
import com.fnzn.fnxf.app.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Administrator
 */
@RestController
@RequestMapping(value="/advert")
public class AdvertController {

    @Autowired
    private AdvertService advertService;

    /**
     * 获取广告栏位图片地址
     * @param type 广告所属的功能类型  1：首页；2：商城页
     * @return
     */
    @RequestMapping(value = "/advertPic", method = { RequestMethod.POST,RequestMethod.GET })
    @ResponseBody
    public JsonResult getAdvertPic(@RequestParam(value = "type")String type){

        List<Advert> advertList =  advertService.selectAdvert(type);

        return JsonResult.success(advertList);
    }

}
