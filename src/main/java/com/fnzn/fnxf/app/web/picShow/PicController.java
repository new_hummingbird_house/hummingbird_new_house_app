package com.fnzn.fnxf.app.web.picShow;

import com.fnzn.fnxf.app.service.mall.MallInfoService;
import com.fnzn.fnxf.app.utils.PybUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
@Api
@Controller
@RequestMapping(value = "/getPic")
public class PicController {

    private final ResourceLoader resourceLoader;

    @Autowired
    public PicController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
    @Value("${upload.path}")
    private String path;
    @Value("${upload.wPath}")
    private String wPath;

    @Autowired
    private MallInfoService mallInfoService;

    /**
     * @return
     */
    @RequestMapping(value = "/**", method = RequestMethod.GET)
    @ApiOperation(value = "图片")
    private ResponseEntity getMallPic(HttpServletRequest request) {

        String uri = request.getRequestURI().replace("/getPic","");
        if(PybUtils.isWindows()) path = wPath;
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(resourceLoader.getResource("file:" + path + uri));
    }

}
