package com.fnzn.fnxf.app.filter;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.core.AppPermissionMapper;
import com.fnzn.fnxf.app.mapper.core.AppRoleMapper;
import com.fnzn.fnxf.app.service.customer.CustomerService;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class MyShiroRealm extends AuthorizingRealm {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AppRoleMapper appRoleMapper;
    @Autowired
    private AppPermissionMapper appPermissionMapper;
    /**
     * 授权用户权限
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        //获取用户
        Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
        SimpleAuthorizationInfo info =  new SimpleAuthorizationInfo();
        //获取用户角色
        Set<String> roleSet = appRoleMapper.selectByUserId(customer.getId());
        info.setRoles(roleSet);
        //获取用户权限
        Set<String> permissionSet = appPermissionMapper.selectByUserId(customer.getId());
        info.setStringPermissions(permissionSet);


        return info;
    }

    /**
     * 验证用户身份
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authcToken) throws AuthenticationException {

        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        String username = token.getUsername();
        String password = token.getPassword()==null?null:String.valueOf(token.getPassword());

        EntityWrapper<Customer> ew = new EntityWrapper<Customer>();
        ew.eq("login_id",username);
        Customer customer = customerService.selectOne(ew);
        if(customer.getPassword()!=null&&!customer.getPassword().equals("")){
            customer.setType("1");
        }else{
            customer.setType("0");
        }
        /**判断是否是首次登陆**/
        if("true".equals(password)){
            customer.setFirst(true);
        }
        /**密码置空**/
        customer.setPassword(password);
        return new SimpleAuthenticationInfo(customer,password, getName());
    }

    /**
     * 权限清空
     */
    public void clearAuthz(){
        this.clearCachedAuthorizationInfo(SecurityUtils.getSubject().getPrincipals());
    }


    /**
     *
     * @Title: clearAuth
     * @Description: TODO 清空所有资源权限
     * @return void    返回类型
     */
    public static void clearAuth(){
        RealmSecurityManager rsm = (RealmSecurityManager)SecurityUtils.getSecurityManager();
        MyShiroRealm realm = (MyShiroRealm)rsm.getRealms().iterator().next();
        realm.clearAuthz();
    }
}
