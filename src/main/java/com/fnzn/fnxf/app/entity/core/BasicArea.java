package com.fnzn.fnxf.app.entity.core;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * BASIC_地区表
 * </p>
 *
 * @author pyb
 * @since 2018-07-27
 */
@TableName("basic_area")
public class BasicArea extends Model<BasicArea> {

    private static final long serialVersionUID = 1L;

    /**
     * 地区ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 全名
     */
    @TableField(value = "fullName")
	private String fullName;
    /**
     * 名称
     */
	private String name;
    /**
     * 路径
     */
	@TableField(value = "treePath")
	private String treePath;
    /**
     * 地区编码
     */
	@TableField(value = "areaCode")
	private String areaCode;
    /**
     * 父节点
     */
	@TableField(value = "parentId")
	private Integer parentId;
    /**
     * 排序
     */
	private Integer orders;
    /**
     * 是否为叶节点
     */
	@TableField(value = "isLeaf")
	private Integer isLeaf;
    /**
     * 创建时间
     */
	@TableField(value = "createDate")
	private Date createDate;
    /**
     * 修改时间
     */
	@TableField(value = "modifyDate")
	private Date modifyDate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTreePath() {
		return treePath;
	}

	public void setTreePath(String treePath) {
		this.treePath = treePath;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getOrders() {
		return orders;
	}

	public void setOrders(Integer orders) {
		this.orders = orders;
	}

	public Integer getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(Integer isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
