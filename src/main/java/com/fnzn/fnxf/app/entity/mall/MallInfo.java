package com.fnzn.fnxf.app.entity.mall;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.math.BigDecimal;


/**
 * @author Administrator
 */
@TableName(value = "mall_info")
public class MallInfo {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String productName;

    private String productBrand;

    private String productModel;

    private String productPic;

    private BigDecimal productPrice;

    private BigDecimal productSalePrice;

    private String productDealer;

    private String productType;

    private BigDecimal productDiscount;

    /**
     * 二维码code
     */
    @TableField("qr_code")
    private String qrCode;

    /**
     * 二维码存储路径
     */
    @TableField("qr_path")
    private String qrPath;

    private String inUse;

    @TableField(value = "discount_price",exist = false)
    private String discountPrice;

    public MallInfo() {
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public BigDecimal getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(BigDecimal productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public BigDecimal getProductPrice() {

        return productPrice;
    }

    public BigDecimal getProductDiscount() {
        
        return productDiscount;
    }

    public void setProductDiscount(BigDecimal productDiscount) {
        this.productDiscount = productDiscount;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDealer() {
        return productDealer;
    }

    public void setProductDealer(String productDealer) {
        this.productDealer = productDealer;
    }

    public String getInUse() {
        return inUse;
    }

    public void setInUse(String inUse) {
        this.inUse = inUse;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrPath() {
        return qrPath;
    }

    public void setQrPath(String qrPath) {
        this.qrPath = qrPath;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }
}
