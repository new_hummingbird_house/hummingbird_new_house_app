package com.fnzn.fnxf.app.entity.voucher;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 代金券申请表
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@TableName("voucher_application")
public class VoucherApplication extends Model<VoucherApplication> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 姓名
     */
	private String name;
    /**
     * 手机号(固定登录账号login_id)
     */
	private String phone;
	/**
	 * 身份证号
	 */
	@TableField("identity_card")
	private String identityCard;
    /**
     * 楼盘ID
     */
	@TableField("building_info_id")
	private Integer buildingInfoId;
	/**
	 * 楼盘名称（非数据库字段）
	 */
	@TableField(exist = false)
	private String buildingTitle;

	@TableField(exist = false)
	private String productName;
    /**
     * 楼号单元房间
     */
	private String apartment;
    /**
     * 顾问ID
     */
	@TableField("adviser_id")
	private Integer adviserId;
    /**
     * 提交日期
     */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	@TableField("submission_time")
	private Date submissionTime;
    /**
     * 审核日期
     */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	@TableField("check_time")
	private Date checkTime;
    /**
     * 申请状态(0:待审核；1:审核通过；2:审核未通过)
     */
	private String status;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 更新时间
     */
	@TableField("update_time")
	private Date updateTime;
    /**
     * 删除标志(正常：0，已删除：1)
     */
	@TableField("del_flag")
	private String delFlag;
    /**
     * 预留字段1
     */
	private String var1;
    /**
     * 预留字段2
     */
	private String var2;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public Integer getBuildingInfoId() {
		return buildingInfoId;
	}

	public void setBuildingInfoId(Integer buildingInfoId) {
		this.buildingInfoId = buildingInfoId;
	}

	public String getBuildingTitle() {
		return buildingTitle;
	}

	public void setBuildingTitle(String buildingTitle) {
		this.buildingTitle = buildingTitle;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public Integer getAdviserId() {
		return adviserId;
	}

	public void setAdviserId(Integer adviserId) {
		this.adviserId = adviserId;
	}

	public Date getSubmissionTime() {
		return submissionTime;
	}

	public void setSubmissionTime(Date submissionTime) {
		this.submissionTime = submissionTime;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getVar1() {
		return var1;
	}

	public void setVar1(String var1) {
		this.var1 = var1;
	}

	public String getVar2() {
		return var2;
	}

	public void setVar2(String var2) {
		this.var2 = var2;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
