package com.fnzn.fnxf.app.entity.user;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 
 * </p>
 *
 * @author pyb
 * @since 2018-07-26
 */
@TableName("user_demand")
public class UserDemand extends Model<UserDemand> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 户型最小面积
     */
	@TableField("min_area")
	private Double minArea;
    /**
     * 最大面积
     */
	@TableField("max_area")
	private Double maxArea;
    /**
     * 希望所在区
     */
    @JsonIgnore
	@TableField("building_area")
	private String buildingArea;
    /**
     * 最小价位
     */
	@TableField("min_price")
	private BigDecimal minPrice;
    /**
     * 最高价位
     */
	@TableField("max_price")
	private BigDecimal maxPrice;
    /**
     * 希望房屋的特色
     */
    @JsonIgnore
	private String advantage;
    /**
     * 需求户型
     */
    @JsonIgnore
	private String apartment;

	@JsonIgnore
	@TableField("create_date")
	private Date createDate;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**所选户型集合**/
	@TableField(exist=false)
	private List<Map<String,String>> apartmentList;

	/**所选周边集合**/
	@TableField(exist = false)
	private List<String> advantageList;

	/**所选区域集合**/
	@TableField(exist = false)
	private List<String> buildingAreaList;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getMinArea() {
		return minArea;
	}

	public void setMinArea(Double minArea) {
		this.minArea = minArea;
	}

	public Double getMaxArea() {
		return maxArea;
	}

	public void setMaxArea(Double maxArea) {
		this.maxArea = maxArea;
	}

	public String getBuildingArea() {
		return buildingArea;
	}

	public void setBuildingArea(String buildingArea) {
		this.buildingArea = buildingArea;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getAdvantage() {
		return advantage;
	}

	public void setAdvantage(String advantage) {
		this.advantage = advantage;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<Map<String, String>> getApartmentList() {
		return apartmentList;
	}

	public void setApartmentList(List<Map<String, String>> apartmentList) {
		this.apartmentList = apartmentList;
	}

	public List<String> getAdvantageList() {
		return advantageList;
	}

	public void setAdvantageList(List<String> advantageList) {
		this.advantageList = advantageList;
	}

	public List<String> getBuildingAreaList() {
		return buildingAreaList;
	}

	public void setBuildingAreaList(List<String> buildingAreaList) {
		this.buildingAreaList = buildingAreaList;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
