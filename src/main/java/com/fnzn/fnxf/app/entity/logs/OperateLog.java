package com.fnzn.fnxf.app.entity.logs;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 员工操作日志
 * </p>
 *
 * @author xiaomh
 * @since 2018-08-03
 */
@TableName("operate_log")
public class OperateLog extends Model<OperateLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 流程ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 操作人
     */
	@TableField("login_id")
	private String loginId;
    /**
     * 操作内容
     */
	private String content;
    /**
     * 原来状态
     */
	@TableField("old_status")
	private Integer oldStatus;
    /**
     * 更新状态
     */
	@TableField("update_status")
	private Integer updateStatus;
    /**
     * 操作表名
     */
	@TableField("table_name")
	private String tableName;
    /**
     * 记录ID
     */
	@TableField("record_id")
	private Integer recordId;
    /**
     * 操作时间
     */
	@TableField("create_date")
	private Date createDate;
    /**
     * 删除标记
     */
	@TableField("del_flag")
	private Integer delFlag;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getOldStatus() {
		return oldStatus;
	}

	public void setOldStatus(Integer oldStatus) {
		this.oldStatus = oldStatus;
	}

	public Integer getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(Integer updateStatus) {
		this.updateStatus = updateStatus;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getRecordId() {
		return recordId;
	}

	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
