package com.fnzn.fnxf.app.entity.user;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 记录用户短信
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@TableName("user_message")
public class UserMessage extends Model<UserMessage> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 发送时间
     */
	@TableField("msg_date")
	private Date msgDate;
    /**
     * 发送地址
     */
	@TableField("msg_address")
	private String msgAddress;
    /**
     * 发送内容
     */
	@TableField("msg_content")
	private String msgContent;
    /**
     * 短信状态，收信或发信
     */
	@TableField("msg_type")
	private Integer msgType;
    /**
     * 用户ID
     */
	@TableField("user_id")
	private Integer userId;
    /**
     * 创建时间
     */
	@TableField("create_date")
	private Date createDate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(Date msgDate) {
		this.msgDate = msgDate;
	}

	public String getMsgAddress() {
		return msgAddress;
	}

	public void setMsgAddress(String msgAddress) {
		this.msgAddress = msgAddress;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
