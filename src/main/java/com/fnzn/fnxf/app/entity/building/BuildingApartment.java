package com.fnzn.fnxf.app.entity.building;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonAnyGetter;

import javax.validation.constraints.NotNull;
import java.util.HashMap;


/* 
* 
* gen by Spring Boot2 Admin 2018-07-03
*/
@TableName(value = "building_apartment")
public class BuildingApartment{

    @NotNull(message = "ID不能为空")
    @TableId(type = IdType.AUTO)
    private Integer id ;
	
    //室的个数

    private Integer room ;
	
    //厅的个数

    private Integer hall ;
	
    //卫的个数

    private Integer hutch ;
	
    //建筑面积

    private Integer coveredArea ;
	
    //户型图

    private String apartmentImg ;
	
    //户型价格，单位为万

    private Long apartmentPrice ;
	
    //户型名字

    private String apartmentName ;
	
    //户型在售状态

    private Integer apartmentState ;
	
    //所属楼盘表ID

    private Integer buildingInfoId ;

    private HashMap extMap;

    @JsonAnyGetter
    public HashMap<String, String> getExtMap() {
        return extMap;
    }

    public void setExtMap(HashMap<String, String> extMap) {
        this.extMap = extMap;
    }
	
    public BuildingApartment()
    {
    }

    public Integer getId(){
	    return  id;
    }
    public void setId(Integer id){
        this.id = id;
    }

    /**室的个数
    *@return 
    */
    public Integer getRoom(){
	    return  room;
    }
    /**室的个数
    *@param  room
    */
    public void setRoom(Integer room){
        this.room = room;
    }

    /**厅的个数
    *@return 
    */
    public Integer getHall(){
	    return  hall;
    }
    /**厅的个数
    *@param  hall
    */
    public void setHall(Integer hall){
        this.hall = hall;
    }

    /**卫的个数
    *@return 
    */
    public Integer getHutch(){
	    return  hutch;
    }
    /**卫的个数
    *@param  hutch
    */
    public void setHutch(Integer hutch){
        this.hutch = hutch;
    }

    /**建筑面积
    *@return 
    */
    public Integer getCoveredArea(){
	    return  coveredArea;
    }
    /**建筑面积
    *@param  coveredArea
    */
    public void setCoveredArea(Integer coveredArea){
        this.coveredArea = coveredArea;
    }

    /**户型图
    *@return 
    */
    public String getApartmentImg(){
	    return  apartmentImg;
    }
    /**户型图
    *@param  apartmentImg
    */
    public void setApartmentImg(String apartmentImg){
        this.apartmentImg = apartmentImg;
    }

    /**户型价格，单位为万
    *@return 
    */
    public Long getApartmentPrice(){
	    return  apartmentPrice;
    }
    /**户型价格，单位为万
    *@param  apartmentPrice
    */
    public void setApartmentPrice(Long apartmentPrice){
        this.apartmentPrice = apartmentPrice;
    }

    /**户型名字
    *@return 
    */
    public String getApartmentName(){
	    return  apartmentName;
    }
    /**户型名字
    *@param  apartmentName
    */
    public void setApartmentName(String apartmentName){
        this.apartmentName = apartmentName;
    }

    /**户型在售状态
    *@return 
    */
    public Integer getApartmentState(){
	    return  apartmentState;
    }
    /**户型在售状态
    *@param  apartmentState
    */
    public void setApartmentState(Integer apartmentState){
        this.apartmentState = apartmentState;
    }

    /**所属楼盘表ID
    *@return 
    */
    public Integer getBuildingInfoId(){
	    return  buildingInfoId;
    }
    /**所属楼盘表ID
    *@param  buildingInfoId
    */
    public void setBuildingInfoId(Integer buildingInfoId){
        this.buildingInfoId = buildingInfoId;
    }


}
