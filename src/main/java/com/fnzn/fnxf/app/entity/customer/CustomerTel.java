package com.fnzn.fnxf.app.entity.customer;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

/**
 * @author Administrator
 */
@TableName(value = "customer_tel")
public class CustomerTel {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String loginId;

    private String realName;

    private String phoneTel;

    private Date telDate;

    public CustomerTel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhoneTel() {
        return phoneTel;
    }

    public void setPhoneTel(String phoneTel) {
        this.phoneTel = phoneTel;
    }

    public Date getTelDate() {
        return telDate;
    }

    public void setTelDate(Date telDate) {
        this.telDate = telDate;
    }
}
