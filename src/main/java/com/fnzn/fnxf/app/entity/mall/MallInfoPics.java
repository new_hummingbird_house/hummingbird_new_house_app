package com.fnzn.fnxf.app.entity.mall;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Administrator
 */
@TableName(value = "mall_info_pics")
public class MallInfoPics {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer mallInfoId;

    private String productPics;

    public MallInfoPics() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMallInfoId() {
        return mallInfoId;
    }

    public void setMallInfoId(Integer mallInfoId) {
        this.mallInfoId = mallInfoId;
    }

    public String getProductPics() {
        return productPics;
    }

    public void setProductPics(String productPics) {
        this.productPics = productPics;
    }
}
