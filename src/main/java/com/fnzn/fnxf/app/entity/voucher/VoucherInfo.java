package com.fnzn.fnxf.app.entity.voucher;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 代金券信息表
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@TableName("voucher_info")
public class VoucherInfo extends Model<VoucherInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;

	/**
	 * 代金券名称
	 */
	@TableField(value="voucher_name")
	private String voucherName;
	/**
	 * 关联ID(代金券申请表主键ID)
	 */
	@TableField("voucher_application_id")
	private Integer voucherApplicationId;
    /**
     * 登录账号(手机号)
     */
	@TableField("login_id")
	private String loginId;
    /**
     * 代金券类型(0：固定商品；1：优惠券)
     */
	private String type;
    /**
     * 商品
     */
	private Integer mallInfoId;
	/**
	 * 商品名称（非数据库字段）
	 */
	@TableField(exist = false)
	private String productName;
    /**
     * 优惠金额
     */
	private BigDecimal preferential;
    /**
     * 起始日期
     */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
	@TableField("start_time")
	private Date startTime;
    /**
     * 截至日期
     */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
	@TableField("due_time")
	private Date dueTime;
    /**
     * 代金券状态(0：未使用；1：已使用；2：已过期)
     */
	private String status;
    /**
     * 二维码路径
     */
	@TableField("qr_path")
	private String qrPath;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 更新时间
     */
	@TableField("update_time")
	private Date updateTime;
    /**
     * 删除标志(正常：0，已删除：1)
     */
	@TableField("del_flag")
	private String delFlag;
    /**
     * 二维码code
     */
	@TableField("qr_code")
	private String qrCode;
    /**
     * 供货商确认人id
     */
	@TableField("dealer_id")
	private String dealerId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVoucherApplicationId() {
		return voucherApplicationId;
	}

	public void setVoucherApplicationId(Integer voucherApplicationId) {
		this.voucherApplicationId = voucherApplicationId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getMallInfoId() {
		return mallInfoId;
	}

	public void setMallInfoId(Integer mallInfoId) {
		this.mallInfoId = mallInfoId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPreferential() {
		return preferential;
	}

	public void setPreferential(BigDecimal preferential) {
		this.preferential = preferential;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getDueTime() {
		return dueTime;
	}

	public void setDueTime(Date dueTime) {
		this.dueTime = dueTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQrPath() {
		return qrPath;
	}

	public void setQrPath(String qrPath) {
		this.qrPath = qrPath;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
