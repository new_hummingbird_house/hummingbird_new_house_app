package com.fnzn.fnxf.app.entity.browsingHistory;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 浏览信息控制表
 * </p>
 *
 * @author xiaomh
 * @since 2018-09-03
 */
@TableName("browsing_history")
public class BrowsingHistory extends Model<BrowsingHistory> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 类别
     */
	private String type;
    /**
     * jsession_id
     */
	@TableField("jsession_id")
	private String jsessionId;
    /**
     * 登录用户
     */
	@TableField("login_id")
	private String loginId;
    /**
     * 时间
     */
	@TableField("browsing_date")
	private Date browsingDate;

	/**
	 * 浏览id
	 */
	@TableField("browsing_id")
	private String browsingId;

	public String getBrowsingId() {
		return browsingId;
	}

	public void setBrowsingId(String browsingId) {
		this.browsingId = browsingId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getJsessionId() {
		return jsessionId;
	}

	public void setJsessionId(String jsessionId) {
		this.jsessionId = jsessionId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Date getBrowsingDate() {
		return browsingDate;
	}

	public void setBrowsingDate(Date browsingDate) {
		this.browsingDate = browsingDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
