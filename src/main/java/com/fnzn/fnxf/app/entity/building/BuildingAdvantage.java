package com.fnzn.fnxf.app.entity.building;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonAnyGetter;

import javax.validation.constraints.NotNull;
import java.util.HashMap;


/* 
* 
* gen by Spring Boot2 Admin 2018-07-04
*/
@TableName(value = "building_advantage")
public class BuildingAdvantage{

    //主键
    @NotNull(message = "ID不能为空")
    @TableId(type = IdType.AUTO)
    private Integer id ;
	
    //楼盘优势名称

    private String advantageType ;
	
    //所属楼盘id

    private Integer buildingInfoId ;
	
    //删除标记
	/*逻辑删除标志*/

    private Integer delFlag ;


    private HashMap<String,String> extMap;

    @JsonAnyGetter
    public HashMap<String, String> getExtMap() {
        return extMap;
    }

    public void setExtMap(HashMap<String, String> extMap) {
        this.extMap = extMap;
    }

    public BuildingAdvantage()
    {
    }

    /**主键
    *@return 
    */
    public Integer getId(){
	    return  id;
    }
    /**主键
    *@param  id
    */
    public void setId(Integer id){
        this.id = id;
    }

    /**楼盘优势名称
    *@return 
    */
    public String getAdvantageType(){
	    return  advantageType;
    }
    /**楼盘优势名称
    *@param  advantageType
    */
    public void setAdvantageType(String advantageType){
        this.advantageType = advantageType;
    }

    /**所属楼盘id
    *@return 
    */
    public Integer getBuildingInfoId(){
	    return  buildingInfoId;
    }
    /**所属楼盘id
    *@param  buildingInfoId
    */
    public void setBuildingInfoId(Integer buildingInfoId){
        this.buildingInfoId = buildingInfoId;
    }

    /**删除标记
    *@return 
    */
    public Integer getDelFlag(){
	    return  delFlag;
    }
    /**删除标记
    *@param  delFlag
    */
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }


}
