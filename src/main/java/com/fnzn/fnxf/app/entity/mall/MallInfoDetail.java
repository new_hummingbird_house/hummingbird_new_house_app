package com.fnzn.fnxf.app.entity.mall;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Administrator
 */
@TableName(value = "mall_info_detail")
public class MallInfoDetail {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String mallInfoId;

    private String productDetailPic;

    private Integer order;

    public MallInfoDetail() {
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getProductDetailPic() {
        return productDetailPic;
    }

    public void setProductDetailPic(String productDetailPic) {
        this.productDetailPic = productDetailPic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMallInfoId() {
        return mallInfoId;
    }

    public void setMallInfoId(String mallInfoId) {
        this.mallInfoId = mallInfoId;
    }
}
