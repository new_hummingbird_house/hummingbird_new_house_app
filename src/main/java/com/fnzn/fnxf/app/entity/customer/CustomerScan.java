package com.fnzn.fnxf.app.entity.customer;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Administrator
 */
@TableName(value = "customer_scan")
public class CustomerScan {

    @NotNull(message = "ID不能为空")
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer buildingInfoId;

    private String loginId;

    private Date scanDate;

    public CustomerScan() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuildingInfoId() {
        return buildingInfoId;
    }

    public void setBuildingInfoId(Integer buildingInfoId) {
        this.buildingInfoId = buildingInfoId;
    }

    public Date getScanDate() {
        return scanDate;
    }

    public void setScanDate(Date scanDate) {
        this.scanDate = scanDate;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
