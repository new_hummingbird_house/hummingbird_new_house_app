package com.fnzn.fnxf.app.entity.building;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;


/* 
* 
* gen by Spring Boot2 Admin 2018-06-29
*/
@TableName(value = "building_info")
public class BuildingInfo {

    //楼盘信息表
    @NotNull(message = "ID不能为空")
    @TableId(type = IdType.AUTO)
    private Integer id ;
	
    //楼盘名称
    private String buildingTitle ;
	
    //楼盘价格
    private BigDecimal buildingPrice ;
	
    //楼盘状态
    private Integer buildingState ;

    //所属省份
    private Integer buildingProvince;

    //所属城市
    private Integer buildingCity;

    //所属县区
    private Integer buildingArea;

    //楼盘详细地址
    private String buildingAddress ;
	
    //楼盘宣传图
    private String buildingImage ;

    //楼盘电话
    private String buildingPhone;
	
    //开盘时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date opDate ;
	
    //入住时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date checkInDate ;
	
    //装修状况：1（毛坯），2（精装）
    private Integer decorateState ;
	
    //容积率
    private BigDecimal plotRatio ;
	
    //绿化率
    private BigDecimal greeningRate ;
	
    //物业管理费
    private Integer propertyCharge ;
	
    //规划户数
    private Integer households ;
	
    //规划面积
    private BigDecimal planArea ;
	
    //建筑面积
    private BigDecimal coveredArea ;
	
    //车位数
    private String numberOfFloors ;
	
    //物业公司
    private String propertyCompany ;
	
    //物业类型
    private Integer propertyType ;
	
    //开发商
    private String developer ;
	
    //产权
    private Integer propertyRight ;
	
    //预售许可证
    private String permitForPresale ;
	
    //删除标记
	/*逻辑删除标志*/
    private Integer delFlag ;

    @TableField(exist=false)
    private HashMap<Object,Object> extMap;

    @JsonAnyGetter
    public HashMap<Object, Object> getExtMap() {
        return extMap;
    }

    public void setExtMap(HashMap<Object, Object> extMap) {
        this.extMap = extMap;
    }

    public BuildingInfo()
    {
    }
    /**楼盘信息表
    *@return 
    */
    public Integer getId(){
	    return  id;
    }
    /**楼盘信息表
    *@param  id
    */
    public void setId(Integer id){
        this.id = id;
    }

    /**楼盘名称
    *@return 
    */
    public String getBuildingTitle(){
	    return  buildingTitle;
    }
    /**楼盘名称
    *@param  buildingTitle
    */
    public void setBuildingTitle(String buildingTitle){
        this.buildingTitle = buildingTitle;
    }

    /**楼盘价格
    *@return 
    */
    public BigDecimal getBuildingPrice(){
	    return  buildingPrice;
    }
    /**楼盘价格
    *@param  buildingPrice
    */
    public void setBuildingPrice(BigDecimal buildingPrice){
        this.buildingPrice = buildingPrice;
    }

    public String getBuildingPhone() {
        return buildingPhone;
    }

    public void setBuildingPhone(String buildingPhone) {
        this.buildingPhone = buildingPhone;
    }

    /**楼盘状态
    *@return 
    */
    public Integer getBuildingState(){
	    return  buildingState;
    }
    /**楼盘状态
    *@param  buildingState
    */
    public void setBuildingState(Integer buildingState){
        this.buildingState = buildingState;
    }

    /**楼盘详细地址
    *@return 
    */
    public String getBuildingAddress(){
	    return  buildingAddress;
    }
    /**楼盘详细地址
    *@param  buildingAddress
    */
    public void setBuildingAddress(String buildingAddress){
        this.buildingAddress = buildingAddress;
    }

    /**楼盘宣传图
    *@return 
    */
    public String getBuildingImage(){
	    return  buildingImage;
    }
    /**楼盘宣传图
    *@param  buildingImage
    */
    public void setBuildingImage(String buildingImage){
        this.buildingImage = buildingImage;
    }

    /**开盘时间
    *@return 
    */
    public Date getOpDate(){
	    return  opDate;
    }
    /**开盘时间
    *@param  opDate
    */
    public void setOpDate(Date opDate){
        this.opDate = opDate;
    }

    /**入住时间
    *@return 
    */
    public Date getCheckInDate(){
	    return  checkInDate;
    }
    /**入住时间
    *@param  checkInDate
    */
    public void setCheckInDate(Date checkInDate){
        this.checkInDate = checkInDate;
    }

    /**装修状况：1（毛坯），2（精装）
    *@return 
    */
    public Integer getDecorateState(){
	    return  decorateState;
    }
    /**装修状况：1（毛坯），2（精装）
    *@param  decorateState
    */
    public void setDecorateState(Integer decorateState){
        this.decorateState = decorateState;
    }

    /**容积率
    *@return 
    */
    public BigDecimal getPlotRatio(){
	    return  plotRatio;
    }
    /**容积率
    *@param  plotRatio
    */
    public void setPlotRatio(BigDecimal plotRatio){
        this.plotRatio = plotRatio;
    }

    /**绿化率
    *@return 
    */
    public BigDecimal getGreeningRate(){
	    return  greeningRate;
    }
    /**绿化率
    *@param  greeningRate
    */
    public void setGreeningRate(BigDecimal greeningRate){
        this.greeningRate = greeningRate;
    }

    /**物业管理费
    *@return 
    */
    public Integer getPropertyCharge(){
	    return  propertyCharge;
    }
    /**物业管理费
    *@param  propertyCharge
    */
    public void setPropertyCharge(Integer propertyCharge){
        this.propertyCharge = propertyCharge;
    }

    /**规划户数
    *@return 
    */
    public Integer getHouseholds(){
	    return  households;
    }
    /**规划户数
    *@param  households
    */
    public void setHouseholds(Integer households){
        this.households = households;
    }

    /**规划面积
    *@return 
    */
    public BigDecimal getPlanArea(){
	    return  planArea;
    }
    /**规划面积
    *@param  planArea
    */
    public void setPlanArea(BigDecimal planArea){
        this.planArea = planArea;
    }

    /**建筑面积
    *@return 
    */
    public BigDecimal getCoveredArea(){
	    return  coveredArea;
    }
    /**建筑面积
    *@param  coveredArea
    */
    public void setCoveredArea(BigDecimal coveredArea){
        this.coveredArea = coveredArea;
    }

    /**车位数
    *@return 
    */
    public String getNumberOfFloors(){
	    return  numberOfFloors;
    }
    /**车位数
    *@param  numberOfFloors
    */
    public void setNumberOfFloors(String numberOfFloors){
        this.numberOfFloors = numberOfFloors;
    }

    /**物业公司
    *@return 
    */
    public String getPropertyCompany(){
	    return  propertyCompany;
    }
    /**物业公司
    *@param  propertyCompany
    */
    public void setPropertyCompany(String propertyCompany){
        this.propertyCompany = propertyCompany;
    }

    /**物业类型
    *@return 
    */
    public Integer getPropertyType(){
	    return  propertyType;
    }
    /**物业类型
    *@param  propertyType
    */
    public void setPropertyType(Integer propertyType){
        this.propertyType = propertyType;
    }

    /**开发商
    *@return 
    */
    public String getDeveloper(){
	    return  developer;
    }
    /**开发商
    *@param  developer
    */
    public void setDeveloper(String developer){
        this.developer = developer;
    }

    /**产权
    *@return 
    */
    public Integer getPropertyRight(){
	    return  propertyRight;
    }
    /**产权
    *@param  propertyRight
    */
    public void setPropertyRight(Integer propertyRight){
        this.propertyRight = propertyRight;
    }

    /**预售许可证
    *@return 
    */
    public String getPermitForPresale(){
	    return  permitForPresale;
    }
    /**预售许可证
    *@param  permitForPresale
    */
    public void setPermitForPresale(String permitForPresale){
        this.permitForPresale = permitForPresale;
    }

    /**删除标记
    *@return 
    */
    public Integer getDelFlag(){
	    return  delFlag;
    }
    /**删除标记
    *@param  delFlag
    */
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }

    /**
     * 所属省份ID
     * @return
     */
    public Integer getBuildingProvince() {
        return buildingProvince;
    }

    /**
     * 所属省份ID
     * @param buildingProvince
     */
    public void setBuildingProvince(Integer buildingProvince) {
        this.buildingProvince = buildingProvince;
    }

    /**
     * 所属城市ID
     * @return
     */
    public Integer getBuildingCity() {
        return buildingCity;
    }

    /**
     * 所属城市ID
     * @param buildingCity
     */
    public void setBuildingCity(Integer buildingCity) {
        this.buildingCity = buildingCity;
    }

    /**
     * 所属县区ID
     * @return
     */
    public Integer getBuildingArea() {
        return buildingArea;
    }

    /**
     * 所属县区ID
     * @param buildingArea
     */
    public void setBuildingArea(Integer buildingArea) {
        this.buildingArea = buildingArea;
    }
}
