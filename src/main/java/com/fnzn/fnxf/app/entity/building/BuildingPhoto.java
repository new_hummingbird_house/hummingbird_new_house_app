package com.fnzn.fnxf.app.entity.building;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName(value = "building_photo")
public class BuildingPhoto {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String photoUrl;

    private String buildingInfoId;

    public String getBuildingInfoId() {
        return buildingInfoId;
    }

    public void setBuildingInfoId(String buildingInfoId) {
        this.buildingInfoId = buildingInfoId;
    }

    public BuildingPhoto() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
