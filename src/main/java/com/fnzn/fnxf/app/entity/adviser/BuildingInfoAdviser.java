package com.fnzn.fnxf.app.entity.adviser;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName(value = "building_adviser")
public class BuildingInfoAdviser {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String adviserId;
    private String buildingInfoId;
    private Integer nowAdviser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdviserId() {
        return adviserId;
    }

    public void setAdviserId(String adviserId) {
        this.adviserId = adviserId;
    }

    public String getBuildingInfoId() {
        return buildingInfoId;
    }

    public void setBuildingInfoId(String buildingInfoId) {
        this.buildingInfoId = buildingInfoId;
    }

    public Integer getNowAdviser() {
        return nowAdviser;
    }

    public void setNowAdviser(Integer nowAdviser) {
        this.nowAdviser = nowAdviser;
    }
}
