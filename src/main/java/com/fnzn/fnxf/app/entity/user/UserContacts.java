package com.fnzn.fnxf.app.entity.user;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户通讯录记录表
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@TableName("user_contacts")
public class UserContacts extends Model<UserContacts> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 通讯录显示名
     */
	@TableField("display_name")
	private String displayName;
    /**
     * 通讯录手机号
     */
	@TableField("phone_number")
	private String phoneNumber;
    /**
     * 用户ID
     */
	@TableField("user_id")
	private Integer userId;
    /**
     * 当前用户的IP地址
     */
	@TableField("ip_address")
	private String ipAddress;
    /**
     * 创建时间
     */
	@TableField("create_date")
	private Date createDate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
