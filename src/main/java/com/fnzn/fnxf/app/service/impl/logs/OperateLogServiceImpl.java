package com.fnzn.fnxf.app.service.impl.logs;

import com.fnzn.fnxf.app.entity.logs.OperateLog;
import com.fnzn.fnxf.app.mapper.logs.OperateLogMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.service.logs.OperateLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工操作日志 服务实现类
 * </p>
 *
 * @author xiaomh
 * @since 2018-08-03
 */
@Service
public class OperateLogServiceImpl extends ServiceImpl<OperateLogMapper, OperateLog> implements OperateLogService {
	
}
