package com.fnzn.fnxf.app.service.impl.consumer;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.bean.consumer.ConsumerComVO;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import com.fnzn.fnxf.app.mapper.consumer.ConsumerCommendMapper;
import com.fnzn.fnxf.app.service.consumer.ConsumerCommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Exception.class)
@Service
public class ConsumerCommendServiceImpl extends ServiceImpl<ConsumerCommendMapper, ConsumerCommend>
        implements ConsumerCommendService {
    @Autowired
    private ConsumerCommendMapper consumerCommendMapper;

    @Override
    public List<ConsumerComVO> findConsumerComVO(Integer userId) {
        return consumerCommendMapper.findConsumerComVO(userId);
    }
}
