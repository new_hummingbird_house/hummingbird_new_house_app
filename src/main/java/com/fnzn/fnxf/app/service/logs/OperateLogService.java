package com.fnzn.fnxf.app.service.logs;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.logs.OperateLog;

/**
 * <p>
 * 员工操作日志 服务类
 * </p>
 *
 * @author xiaomh
 * @since 2018-08-03
 */
public interface OperateLogService extends IService<OperateLog> {
	
}
