package com.fnzn.fnxf.app.service.impl.advert;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.advert.Advert;
import com.fnzn.fnxf.app.mapper.advert.AdvertMapper;
import com.fnzn.fnxf.app.service.advert.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Service
public class AdvertServiceImpl extends ServiceImpl<AdvertMapper,Advert> implements AdvertService {

    @Autowired
    private AdvertMapper advertMapper;


    @Override
    public List<Advert> selectAdvert(String type) {

        Map<String, Object> map = new HashMap<String, Object>(2);
        map.put("in_use","1");
        map.put("type",type);

        List<Advert> advertList = advertMapper.selectByMap(map);

        return advertList;
    }
}
