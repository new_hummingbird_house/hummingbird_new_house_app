package com.fnzn.fnxf.app.service.impl.building;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.building.Information;
import com.fnzn.fnxf.app.mapper.building.InformationMapper;
import com.fnzn.fnxf.app.service.building.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = Exception.class)
@Service
public class InformationServiceImpl extends ServiceImpl<InformationMapper, Information>
        implements InformationService {
    @Autowired
    private InformationMapper informationMapper;

    /**
     * 查询并更新阅读量
     * @param id 资讯ID
     * @return 资讯
     */
    @Override
    public Information selectAndUpdateReadNum(Integer id){
        Information information = informationMapper.selectById(id);
        information.setReadNum(information.getReadNum()+1);
        Integer successNum = informationMapper.updateById(information);
        if(successNum>0){
            return information;
        }else{
            return null;
        }
    }
}
