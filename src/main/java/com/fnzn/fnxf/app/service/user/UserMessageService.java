package com.fnzn.fnxf.app.service.user;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.user.UserMessage;

/**
 * <p>
 * 记录用户短信 服务类
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
public interface UserMessageService extends IService<UserMessage> {
	public boolean recordUserMessage(String message,Integer userId);
}
