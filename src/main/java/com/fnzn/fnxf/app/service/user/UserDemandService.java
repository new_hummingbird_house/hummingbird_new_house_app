package com.fnzn.fnxf.app.service.user;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.user.UserDemand;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pyb
 * @since 2018-07-26
 */
public interface UserDemandService extends IService<UserDemand> {
    public UserDemand selectUserDemand(Integer userId);
    public Map<String,Object> userDemandDict();
}
