package com.fnzn.fnxf.app.service.mall;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.mall.MallInfoListVO;
import com.fnzn.fnxf.app.bean.mall.MallProductBuyVO;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.google.zxing.WriterException;

import java.io.IOException;

/**
 * @author Administrator
 */
public interface MallInfoService extends IService<MallInfo> {


    MallProductBuyVO selectMallProductById(String id);

    /**
     * 获取商城列表信息
     * @return
     */
    MallInfoListVO selectMallInfoList(String type, String search);

    String gerQrCode() throws IOException, WriterException;
}
