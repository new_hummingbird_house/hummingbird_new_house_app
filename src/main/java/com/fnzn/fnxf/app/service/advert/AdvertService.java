package com.fnzn.fnxf.app.service.advert;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.advert.Advert;

import java.util.List;

/**
 * @author Administrator
 */
public interface AdvertService extends IService<Advert> {

    /**
     * @param type
     * @return
     */
    List<Advert> selectAdvert(String type);
}
