package com.fnzn.fnxf.app.service.impl.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.entity.core.BasicArea;
import com.fnzn.fnxf.app.entity.core.CoreDict;
import com.fnzn.fnxf.app.entity.user.UserDemand;
import com.fnzn.fnxf.app.mapper.core.BasicAreaMapper;
import com.fnzn.fnxf.app.mapper.core.CoreDictMapper;
import com.fnzn.fnxf.app.mapper.user.UserDemandMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.service.user.UserDemandService;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pyb
 * @since 2018-07-26
 */
@Service
public class UserDemandServiceImpl extends ServiceImpl<UserDemandMapper, UserDemand> implements UserDemandService {

    @Autowired
    private UserDemandMapper userDemandMapper;

    @Autowired
    private CoreDictMapper coreDictMapper;

    @Autowired
    private BasicAreaMapper basicAreaMapper;

    @Override
    public UserDemand selectUserDemand(Integer userId){
        UserDemand userDemand = new UserDemand();
        userDemand.setUserId(userId);
        userDemand = userDemandMapper.selectOne(userDemand);
        if(userDemand==null){
            return null;
        }else{
            String apartment = userDemand.getApartment();
            String advantage = userDemand.getAdvantage();
            String buildingArea = userDemand.getBuildingArea();
            List<Map<String,String>> apartmentList = new ArrayList<>();
            List<String> advantageList = new ArrayList<>();
            List<String> buildingAreaList = new ArrayList<>();
            /**分割为集合**/
            if(PybUtils.objectIsNotEmpty(advantage)){
                String[] advantageArr = advantage.split(",");
                advantageList = userDemandMapper.selectAdvantageListInType(advantageArr);
            }
            /**分割为集合**/
            if(PybUtils.objectIsNotEmpty(buildingArea)){
                String[] buildingAreaArr = buildingArea.split(",");
                buildingAreaList =userDemandMapper.selectBuildingAreaList(buildingAreaArr);
            }
            /**分割为集合**/
            if(PybUtils.objectIsNotEmpty(apartment)){
                String[] apartmentArr = apartment.split(",");
                for(String str : apartmentArr){
                    String[] aptArr = str.split("-");
                    int i = 0;
                    Map<String,String> map = new HashMap<>();
                    for(String aptStr : aptArr){
                        switch (i){
                            case 0:
                                map.put("room",aptStr);
                                break;
                            case 1:
                                map.put("hall",aptStr);
                                break;
                            case 2:
                                map.put("hutch",aptStr);
                                break;
                        }
                        i++;
                    }
                    apartmentList.add(map);
                }
            }

            userDemand.setAdvantageList(advantageList);
            userDemand.setApartmentList(apartmentList);
            userDemand.setBuildingAreaList(buildingAreaList);

            return  userDemand;
        }
    }

    @Override
    public Map<String,Object> userDemandDict(){
        Map<String,Object> result = new HashMap<>();
        List<CoreDict> coreDictList = coreDictMapper.selectList(
                new EntityWrapper<CoreDict>()
                        .eq("TYPE", "advantage_type")
                        .eq("DEL_FLAG", "0")
        );
        List<BasicArea> basicAreaList = basicAreaMapper.selectList(
                new EntityWrapper<BasicArea>()
                    .eq("parentId","1417")
        );
        result.put("advantage",coreDictList);
        result.put("buildingArea",basicAreaList);
        return  result;
    }

}
