package com.fnzn.fnxf.app.service.customer;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.core.RolePermissionVO;
import com.fnzn.fnxf.app.entity.customer.Customer;

public interface CustomerService extends IService<Customer> {
    public RolePermissionVO findCustomerRolAndPer(Customer customer);
    public boolean updateCustomerInfo(Customer newCustomer,Integer buildingInfoId);
}
