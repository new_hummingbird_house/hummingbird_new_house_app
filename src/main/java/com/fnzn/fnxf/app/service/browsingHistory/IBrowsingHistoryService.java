package com.fnzn.fnxf.app.service.browsingHistory;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.browsingHistory.BrowsingHistory;

import java.util.List;

/**
 * <p>
 * 浏览信息控制表 服务类
 * </p>
 *
 * @author xiaomh
 * @since 2018-09-03
 */
public interface IBrowsingHistoryService extends IService<BrowsingHistory> {

    List<BrowsingHistory> checkBrowsingHistory(String id);
	
}
