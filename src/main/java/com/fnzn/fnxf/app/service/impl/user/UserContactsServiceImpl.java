package com.fnzn.fnxf.app.service.impl.user;

import com.fnzn.fnxf.app.entity.user.UserContacts;
import com.fnzn.fnxf.app.mapper.user.UserContactsMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.service.user.UserContactsService;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户通讯录记录表 服务实现类
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@Service
public class UserContactsServiceImpl extends ServiceImpl<UserContactsMapper, UserContacts> implements UserContactsService {

    @Override
    public boolean recordUserContacts(String contacts,String ipAddress,Integer userId) {
        List<UserContacts> userContactsList = PybUtils.jsonToListObj(contacts, UserContacts.class);
        if(userContactsList!=null&&userContactsList.size()>0){
            for(UserContacts userContacts:userContactsList){
                userContacts.setIpAddress(ipAddress);
                userContacts.setUserId(userId);
            }
            this.insertBatch(userContactsList);
            return true;
        }else{
            return false;
        }
    }
}
