package com.fnzn.fnxf.app.service.impl.user;

import com.fnzn.fnxf.app.entity.user.UserMessage;
import com.fnzn.fnxf.app.mapper.user.UserMessageMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.service.user.UserMessageService;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 记录用户短信 服务实现类
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
@Service
public class UserMessageServiceImpl extends ServiceImpl<UserMessageMapper, UserMessage> implements UserMessageService {

    @Override
    public boolean recordUserMessage(String message, Integer userId) {
        List<UserMessage> userMessageList = PybUtils.jsonToListObj(message, UserMessage.class);
        if(userMessageList!=null&&userMessageList.size()>0){
            for(UserMessage userMessage:userMessageList){
                userMessage.setUserId(userId);
            }
            this.insertBatch(userMessageList);
            return true;
        }else{
            return false;
        }
    }
}
