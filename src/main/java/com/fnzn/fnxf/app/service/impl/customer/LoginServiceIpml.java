package com.fnzn.fnxf.app.service.impl.customer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;
import com.fnzn.fnxf.app.mapper.customer.LoginMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoMapper;
import com.fnzn.fnxf.app.mapper.voucher.VoucherInfoMapper;
import com.fnzn.fnxf.app.service.customer.LoginService;
import com.fnzn.fnxf.app.utils.*;
import com.google.zxing.WriterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Service
public class LoginServiceIpml implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private MallInfoMapper mallInfoMapper;

    @Autowired
    private VoucherInfoMapper voucherInfoMapper;

    /**
     * @param loginId
     * @param password
     * @return
     */
    @Override
    public Map<String, Object> loginforps(String loginId, String password) {

        Map<String, Object> map = new HashMap<String, Object>();

        List<Customer> list = loginMapper.selectByLoginId(loginId);
        Customer customer = new Customer();

        if (list == null || list.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "您尚未注册，请使用验证码登录！");
        } else {
            password = MD5Utils.MD5Encode(MD5Utils.MD5Encode(password, "UTF-8"), "UTF-8");
            Map<String, Object> map1 = new HashMap<String, Object>();
            map1.put("loginId", loginId);
            map1.put("password", password);
            list = loginMapper.checkPassWord(map1);
            if (list == null || list.isEmpty()) {
                map.put("code", 1);
                map.put("msg", "密码错误，请重新输入！");
            } else {
                customer = list.get(0);
                customer.setType("1");
                map.put("code", 0);
                map.put("customer", customer);
            }
        }

        return map;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> loginforcode(String loginId) throws IOException, WriterException {

        Map<String, Object> map = new HashMap<String, Object>();
        Customer customer = new Customer();

        //判断该用户是否已存在
        List<Customer> list = loginMapper.selectByLoginId(loginId);
        //用户不存在
        if (list == null || list.isEmpty()) {
            //保存用户登录信息
            customer.setLoginId(loginId);
            customer.setNickName(loginId + "_" + new Random().nextInt(1000));
            customer.setRegisterDate(new Date());
            customer.setRoleId(1);
            customer.setPhotoUrl(Constants.CUSTOMERPIC);
            loginMapper.insert(customer);
            customer.setType("0");
            customer.setFirst(true);
            //首次注册登录，发放代金券,每件商品一个优惠券，且优惠金额不同
            EntityWrapper<MallInfo> entityWrapper = new EntityWrapper<>();
            entityWrapper.eq("in_use", "1");
            List<MallInfo> mallInfoList = mallInfoMapper.selectList(entityWrapper);
            if (mallInfoList != null && !mallInfoList.isEmpty()) {
                for (int i = 0; i < mallInfoList.size(); i++) {
                    UUID uuid = UUID.randomUUID();
                    String fileName = uuid.toString().replace("-", "").substring(0, 10);
                    String format = "jpg";
                    String outFileUrl = Constants.QRPATH + fileName + "." + format;

                    MallInfo mallInfo = mallInfoList.get(i);
                    VoucherInfo voucherInfo = new VoucherInfo();
                    voucherInfo.setLoginId(customer.getLoginId());
                    voucherInfo.setType(Constants.VOUCHER_COUPON);
                    voucherInfo.setMallInfoId(mallInfo.getId());
                    voucherInfo.setVoucherName("代金券：省" + mallInfo.getProductDiscount() + "元");
                    voucherInfo.setPreferential(mallInfo.getProductDiscount());
                    voucherInfo.setStartTime(new Date());
                    voucherInfo.setDueTime(getAfterThreeMonthsDate());
                    voucherInfo.setStatus(Constants.STATUS_UNUSED);
                    voucherInfo.setQrPath("/qrCode/" + fileName + "." + format);
                    voucherInfo.setCreateTime(new Date());
                    voucherInfo.setDelFlag(Constants.STATUS_NORMAL);
                    voucherInfo.setQrCode(fileName);
                    voucherInfoMapper.add(voucherInfo);
                    Integer voucherInfoId = voucherInfo.getId();

                    String content = String.valueOf(voucherInfoId) + ";" + customer.getLoginId();
                    content = SymmetricEncoder.AESEncode(Constants.ENCODERULES, content);
                    QRCodeFactory qrCodeFactory = new QRCodeFactory();
                    qrCodeFactory.CreatQrImage(content, format, outFileUrl, null, null);

                }
            }
        } else {
            //用户存在
            customer = list.get(0);
            customer.setType("1");
            customer.setFirst(false);
        }
        map.put("code", 0);
        map.put("customer", customer);

        return map;
    }


    /**
     * 计算截止日期(三月后)
     *
     * @return
     */
    public Date getAfterThreeMonthsDate() {
        Date nowDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);
        //截止日期 三个月
        calendar.add(Calendar.MONTH, Constants.DUETIME);
        Date afterDate = calendar.getTime();
        return afterDate;
    }

    @Override
    public String getCode(String loginId, String type) {
        String code = SendSmsUtil.getCaptcha();
        //短息模板ID
        String templateCode = "";
        if ("1".equals(type)) {
            //用户注册、登录验证码
            templateCode = Constants.CAPTCHA_TEL_CODE_LOGIN;
        } else if ("2".equals(type)) {
            //用户购买信息确认短信验证码
            templateCode = Constants.CAPTCHA_TEL_CODE_CONFIRM;
        }

        String templateParam = "{\"" + Constants.CODE_CAPTCHA_VAR_NAME + "\":\"" + code + "\"}";
        String a = SendSmsUtil.sendSms(templateCode, loginId, templateParam);
        String flag = "fail";
        if (flag.equals(a)) {
            return "";
        } else {
            return code;
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            UUID uuid = UUID.randomUUID();
            String fileName = uuid.toString().replace("-", "");
            System.out.println(fileName);
            System.out.println(new Date().toString());
        }

    }

}
