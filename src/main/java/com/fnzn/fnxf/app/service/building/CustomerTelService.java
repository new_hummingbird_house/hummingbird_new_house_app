package com.fnzn.fnxf.app.service.building;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.customer.CustomerTel;

/**
 * @author Administrator
 */
public interface CustomerTelService extends IService<CustomerTel> {

    void insertTelHistory(String phone,Customer customer);
}
