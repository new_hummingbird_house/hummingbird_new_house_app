package com.fnzn.fnxf.app.service.impl.voucher;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.voucher.VoucherApplication;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;
import com.fnzn.fnxf.app.mapper.customer.CustomerMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoMapper;
import com.fnzn.fnxf.app.mapper.voucher.VoucherApplicationMapper;
import com.fnzn.fnxf.app.mapper.voucher.VoucherInfoMapper;
import com.fnzn.fnxf.app.service.voucher.IVoucherInfoService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.SymmetricEncoder;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 代金券信息表 服务实现类
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@Service
public class VoucherInfoServiceImpl extends ServiceImpl<VoucherInfoMapper, VoucherInfo> implements IVoucherInfoService {

    @Autowired
    private VoucherInfoMapper voucherInfoMapper;

    @Autowired
    private VoucherApplicationMapper voucherApplicationMapper;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private MallInfoMapper mallInfoMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<VoucherInfoVO> selectVoucherInfoByStatus(String status) {

        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        String dutDate = sd.format(new Date());
        voucherInfoMapper.updateByDueTime(customer.getLoginId(),dutDate);

        List<VoucherInfoVO> voucherInfoVO = voucherInfoMapper.selectVoucherInfoByStatus(customer.getLoginId(),status);

        return voucherInfoVO;
    }

    @Override
    public VoucherInfoVO selectVoucherDetailById(String id) {
        VoucherInfoVO voucherInfoVO = voucherInfoMapper.selectVoucherDetailById(Integer.valueOf(id));
        return voucherInfoVO;
    }

    @Override
    public Map<String,Object> voucherUseForBusiness(String content) {
        Map<String,Object> map = new HashMap<String,Object>();

        content = SymmetricEncoder.AESDncode(Constants.ENCODERULES, content);
        String[] args = content.split(";");
        String id = args[0];
        String loginId = args[1];
        VoucherInfoVO voucherInfoVO = new VoucherInfoVO();
        if(!id.equals("0")){
            //兑换类优惠券或者代金券
            //验证优惠券二维码中id与LoginId是否对应
            EntityWrapper<VoucherInfo> entityWrapper = new EntityWrapper<VoucherInfo>();
            entityWrapper.eq("id",id);
            entityWrapper.eq("login_id",loginId);
            Integer count = voucherInfoMapper.selectCount(entityWrapper);
            if("0".equals(count)){
                map.put("msg","该代金券信息错误，请联系管理员！");
                return map;
            }

            //刷新代金券状态
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
            String dutDate = sd.format(new Date());
            voucherInfoMapper.updateByDueTime(loginId,dutDate);

            //获取代金券信息
            VoucherApplication voucherApplication = new VoucherApplication();
            voucherInfoVO = voucherInfoMapper.selectVoucherDetailById(Integer.valueOf(id));
            if(voucherInfoVO==null){
                map.put("msg","该代金券信息不存在，请联系管理员！");
                return map;
            }
            //判断代金券的状态
            String status = voucherInfoVO.getStatus();
            if(StringUtils.isEmpty(status)){
                map.put("msg","该代金券状态不正常，请联系管理员！");
                return map;
            }else{
                if(status.equals("1")){
                    map.put("msg","该代金券已使用！");
                    return map;
                }else if(status.equals("2")){
                    map.put("msg","该代金券已过期！");
                    return map;
                }
            }

            /**
             *   代金券状态未使用时，获取申请id，以获取申请人姓名，
             *   若该代金券为抵扣部分金额时，姓名为该顾客的个人信息维护的真实姓名，若真实姓名为空，为用户昵称
             *   若该代金券为兑换券时，姓名为用户申请兑换券的姓名
             */
            Integer voucherApplicationId = voucherInfoVO.getVoucherApplicationId();
            if(voucherApplicationId==null||voucherApplicationId.equals("")){
                Customer customer = new Customer();
                customer.setLoginId(loginId);
                customer = customerMapper.selectOne(customer);
                String realName = customer.getRealName();
                if(StringUtils.isEmpty(realName)){
                    realName = customer.getNickName();
                }
                voucherInfoVO.setName(realName);
            }else{
                voucherApplication = voucherApplicationMapper.selectById(voucherApplicationId);
                voucherInfoVO.setIdentityCard(voucherApplication.getIdentityCard());
                voucherInfoVO.setName(voucherApplication.getName());
            }
        }else{
            //正常购买，不使用优惠券,此时loginId为商品二维码号码
            VoucherInfo voucherInfo = new VoucherInfo();
            MallInfo mallInfo = new MallInfo();
            mallInfo.setQrCode(loginId);
            mallInfo = mallInfoMapper.selectOne(mallInfo);
            voucherInfo.setLoginId("00000000000");
            voucherInfo.setVoucherName("正常购买，无优惠方式");
            voucherInfo.setQrCode(loginId);
            voucherInfo.setStatus(Constants.STATUS_UNUSED);
            voucherInfo.setDelFlag(Constants.STATUS_NORMAL);
            voucherInfo.setMallInfoId(mallInfo.getId());
            voucherInfo.setPreferential(new BigDecimal("0"));
            voucherInfo.setStartTime(new Date());
            voucherInfo.setDueTime(getAfterThreeMonthsDate());
            voucherInfo.setQrPath(mallInfo.getQrPath());
            voucherInfo.setType(null);
            voucherInfo.setVoucherApplicationId(null);
            voucherInfoMapper.add(voucherInfo);

            voucherInfoVO.setId(voucherInfo.getId());
            voucherInfoVO.setName("无优惠购买用户");
            voucherInfoVO.setLoginId("00000000000");
            voucherInfoVO.setIdentityCard("");
            voucherInfoVO.setProductName(mallInfo.getProductName());
            voucherInfoVO.setProductModel(mallInfo.getProductModel());
            voucherInfoVO.setProductSalePrice(mallInfo.getProductSalePrice());
            voucherInfoVO.setPreferential(new BigDecimal("0"));
            voucherInfoVO.setProductPayPrice(mallInfo.getProductSalePrice().toString());
            voucherInfoVO.setProductDealer(mallInfo.getProductDealer());

        }

        map.put("msg","");
        map.put("VO",voucherInfoVO);
        return map;
    }

    @Override
    public Map<String, Object> voucherCheckPass(String id) {
        Map<String,Object> map = new HashMap<String,Object>();

        VoucherInfo voucherInfo = voucherInfoMapper.selectById(Integer.valueOf(id));

        String status = voucherInfo.getStatus();
        if(!status.equals("0")){
            map.put("msg","该代金券状态不正常，请联系管理员!");
            map.put("code","201");
            return map;
        }
        if(StringUtils.isNotEmpty(voucherInfo.getDealerId())){
            map.put("msg","该代金券已使用，请联系管理员!");
            map.put("code","202");
            return map;
        }

        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();

        voucherInfo.setStatus("1");
        voucherInfo.setDealerId(customer.getLoginId());
        voucherInfoMapper.updateById(voucherInfo);

        map.put("msg","操作成功!");
        map.put("code","200");
        return map;
    }

    @Override
    public Map<String, Object> getVoucherInfo(String qrCode) {
        Map<String,Object> map = new HashMap<String,Object>();

        VoucherInfo voucherInfo = new VoucherInfo();
        if(qrCode.startsWith("fnsc")){
            voucherInfo.setId(0);
            voucherInfo.setLoginId(qrCode);
        }else{
            voucherInfo.setQrCode(qrCode);
            voucherInfo = voucherInfoMapper.selectOne(voucherInfo);

            if(voucherInfo.getId()==null||voucherInfo.getId().equals("0")){
                map.put("code","500");
                map.put("msg","您输入的二维码编号不正确！");
                return map;
            }
        }

        String content = voucherInfo.getId()+";"+voucherInfo.getLoginId();
        String id = SymmetricEncoder.AESEncode(Constants.ENCODERULES,content);

        map.put("code","200");
        map.put("msg","成功！");
        map.put("date",id);



        return map;


    }

    /**
     * 计算截止日期(三月后)
     *
     * @return
     */
    public Date getAfterThreeMonthsDate() {
        Date nowDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);
        //截止日期 三个月
        calendar.add(Calendar.MONTH, Constants.DUETIME);
        Date afterDate = calendar.getTime();
        return afterDate;
    }

}
