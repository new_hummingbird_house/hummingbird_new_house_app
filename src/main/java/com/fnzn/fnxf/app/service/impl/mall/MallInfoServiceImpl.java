package com.fnzn.fnxf.app.service.impl.mall;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.bean.mall.MallInfoListVO;
import com.fnzn.fnxf.app.bean.mall.MallProductBuyVO;
import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.entity.advert.Advert;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;
import com.fnzn.fnxf.app.mapper.advert.AdvertMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoMapper;
import com.fnzn.fnxf.app.mapper.voucher.VoucherInfoMapper;
import com.fnzn.fnxf.app.service.mall.MallInfoService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.JsonResult;
import com.fnzn.fnxf.app.utils.QRCodeFactory;
import com.fnzn.fnxf.app.utils.SymmetricEncoder;
import com.google.zxing.WriterException;
import io.swagger.models.auth.In;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

/**
 * @author Administrator
 */
@Service
public class MallInfoServiceImpl extends ServiceImpl<MallInfoMapper, MallInfo> implements MallInfoService {

    @Autowired
    private MallInfoMapper mallInfoMapper;

    @Autowired
    private VoucherInfoMapper voucherInfoMapper;

    @Override
    public MallProductBuyVO selectMallProductById(String id) {
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();

        MallInfo mallInfo = mallInfoMapper.selectById(Integer.valueOf(id));

        Map<String,Object> map = new HashMap<String,Object>(2);
        map.put("id",Integer.valueOf(id));
        map.put("loginId",customer.getLoginId());
        List<VoucherInfoVO> voucherInfoVOList = voucherInfoMapper.selectVoucherDetailByCustomer(map);

        MallProductBuyVO mallProductBuyVO = new MallProductBuyVO();
        mallProductBuyVO.setMallInfo(mallInfo);
        mallProductBuyVO.setVoucherInfoVOList(voucherInfoVOList);

        return mallProductBuyVO;
    }

    @Override
    public MallInfoListVO selectMallInfoList(String type, String search) {
        List<MallInfo> mallInfoList = new ArrayList<MallInfo>();
        Map<String,Object> map = new HashMap<String,Object>(2);
        map.put("type",type);
        map.put("name","%" + search + "%");
        mallInfoList =  mallInfoMapper.selectBySearch(map);

        MallInfoListVO mallInfoListVO = new MallInfoListVO();
        mallInfoListVO.setMallInfoLists(mallInfoList);

        return mallInfoListVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String gerQrCode() throws IOException, WriterException {
        EntityWrapper<MallInfo> entityWrapper = new EntityWrapper<>();
        entityWrapper.eq("in_use", "1");
        List<MallInfo> mallInfoList = mallInfoMapper.selectList(entityWrapper);
        if (mallInfoList != null && !mallInfoList.isEmpty()) {
            for (int i = 0; i < mallInfoList.size(); i++) {
                MallInfo mallInfo = mallInfoList.get(i);
                String format = "jpg";
                UUID uuid = UUID.randomUUID();
                String fileName = uuid.toString().replace("-", "").substring(0, 10);
                mallInfo.setQrCode(fileName);
                mallInfo.setQrPath("/qrCode/product/"+ fileName + "." + format);
                mallInfoMapper.updateById(mallInfo);
                String outFileUrl = "c:/"+Constants.QRPATH + "product/" + fileName + "." + format;
                String content =  "0;" + fileName;
                content = SymmetricEncoder.AESEncode(Constants.ENCODERULES, content);
                QRCodeFactory qrCodeFactory = new QRCodeFactory();
                qrCodeFactory.CreatQrImage(content, format, outFileUrl, null, null);
            }
        }
        return "000";
    }

}
