package com.fnzn.fnxf.app.service.consumer;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.consumer.ReportDictVO;
import com.fnzn.fnxf.app.bean.consumer.StaffReportVO;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import java.util.List;
import java.util.Map;

public interface StaffReportService extends IService<StaffReport> {

    public List<StaffReportVO> findStaffReportVO(Integer userId);

    public ReportDictVO findReportDict();

    public Map<String,Object> findAdviserOneByBuildingId(Integer buildingId);

    public boolean adviserRestartSort(Integer adviserId,Integer buildingInfoId);
}
