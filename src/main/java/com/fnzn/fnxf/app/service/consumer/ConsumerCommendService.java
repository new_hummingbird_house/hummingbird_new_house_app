package com.fnzn.fnxf.app.service.consumer;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.consumer.ConsumerComVO;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import java.util.List;

public interface ConsumerCommendService extends IService<ConsumerCommend> {
    public List<ConsumerComVO> findConsumerComVO(Integer userId);
}
