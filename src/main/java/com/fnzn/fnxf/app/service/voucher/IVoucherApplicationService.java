package com.fnzn.fnxf.app.service.voucher;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.voucher.VoucherApplication;

import java.util.List;

/**
 * <p>
 * 代金券申请表 服务类
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
public interface IVoucherApplicationService extends IService<VoucherApplication> {

    /**
     *
     * @param status
     * @param loginId
     * @return
     */
    List<VoucherApplication> getVoucherApplicationToCheck(String status, String loginId);

    /**
     *
     * @param pass
     * @param fail
     * @param loginId
     * @return
     */
    List<VoucherApplication> getVoucherApplicationChecked(String pass, String fail, String loginId);

}
