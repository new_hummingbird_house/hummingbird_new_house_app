package com.fnzn.fnxf.app.service.consumer;

import com.fnzn.fnxf.app.entity.logs.OperateLog;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface EmployeeFollowService {
    public List<Map<String,Object>> findMyCustomer(String phone);
    public Map<String,Object> getMyCustomerById(Integer sourceType,Integer id,String phone);
    public boolean updateProgressStatus(OperateLog operateLog, Integer sourceType, Integer sourceId, String phone);
    public boolean isSamePhone(String phone,String sparePhone,Integer buildingInfoId);
}
