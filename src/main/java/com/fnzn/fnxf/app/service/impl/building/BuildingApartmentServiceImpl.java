package com.fnzn.fnxf.app.service.impl.building;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.building.BuildingApartment;
import com.fnzn.fnxf.app.mapper.building.BuildingApartmentMapper;
import com.fnzn.fnxf.app.service.building.BuildingApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Exception.class)
@Service
public class BuildingApartmentServiceImpl extends ServiceImpl<BuildingApartmentMapper, BuildingApartment>
        implements BuildingApartmentService {

    @Autowired
    BuildingApartmentMapper buildingApartmentMapper;

    @Override
    public List<BuildingApartment> selectApartmentByBuildingId(String id) {

        List<BuildingApartment> buildingApartments = buildingApartmentMapper.selectListByBuildingId(Integer.valueOf(id));
        return buildingApartments;
    }
}
