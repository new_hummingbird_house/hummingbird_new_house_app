package com.fnzn.fnxf.app.service.customer;

import com.google.zxing.WriterException;

import java.io.IOException;
import java.util.Map;

public interface LoginService {

    Map<String,Object> loginforps(String phone, String password);

    Map<String,Object> loginforcode(String phone) throws IOException, WriterException;

    String getCode(String loginId,String type);
}
