package com.fnzn.fnxf.app.service.voucher;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代金券信息表 服务类
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
public interface IVoucherInfoService extends IService<VoucherInfo> {

    /**
     * @param status
     * @return
     */
    List<VoucherInfoVO> selectVoucherInfoByStatus(String status);

    /**
     * @param id
     * @return
     */
    VoucherInfoVO selectVoucherDetailById(String id);

    /**
     * @param content
     * @return
     */
    Map<String,Object> voucherUseForBusiness(String content);

    /**
     * @param id
     * @return
     */
    Map<String,Object> voucherCheckPass(String id);

    /**
     * @param qrCode
     * @return
     */
    Map<String,Object> getVoucherInfo(String qrCode);

}
