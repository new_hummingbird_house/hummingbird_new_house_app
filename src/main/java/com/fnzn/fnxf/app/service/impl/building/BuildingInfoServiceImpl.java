package com.fnzn.fnxf.app.service.impl.building;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.building.*;
import com.fnzn.fnxf.app.bean.building.*;
import com.fnzn.fnxf.app.entity.core.AppRole;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.customer.CustomerScan;
import com.fnzn.fnxf.app.mapper.building.*;
import com.fnzn.fnxf.app.mapper.core.AppRoleMapper;
import com.fnzn.fnxf.app.mapper.customer.CustomerMapper;
import com.fnzn.fnxf.app.service.building.BuildingInfoService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BuildingInfoServiceImpl extends ServiceImpl<BuildingInfoMapper, BuildingInfo>
        implements BuildingInfoService {

    @Autowired
    BuildingInfoMapper buildingInfoMapper;

    @Autowired
    BuildingAdviserMapper buildingAdviserMapper;

    @Autowired
    BuildingAdvantageMapper buildingAdvantageMapper;

    @Autowired
    BuildingApartmentMapper buildingApartmentMapper;

    @Autowired
    CustomerScanMapper customerScanMapper;

    @Autowired
    AppRoleMapper appRoleMapper;

    @Autowired
    CustomerMapper customerMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public BuildingInfoDetailsVO buildingDetails(String id, Customer customer) {
        BuildingInfoDetailsVO buildingInfoDetailsVO = buildingInfoMapper.BuildingInfoDetailsById(Integer.valueOf(id));

        List<BuildingAdviser> buildingAdviserList = buildingAdviserMapper.selectAdviserListByBuildingId(Integer.valueOf(id));
        buildingInfoDetailsVO.setBuildingAdviserList(buildingAdviserList);

        List<BuildingPhoto> buildingPhotoList = buildingInfoMapper.selectBuildingPhotoByBuildingId(Integer.valueOf(id));
        buildingInfoDetailsVO.setBuildingPhotoList(buildingPhotoList);

        List<BuildingAdvantage> buildingAdvantageList = buildingAdvantageMapper.selectAdvantageListByBuildingId(Integer.valueOf(id));
        buildingInfoDetailsVO.setBuildingAdvantageList(buildingAdvantageList);

        List<BuildingApartment> buildingApartmentList = buildingApartmentMapper.selectListByBuildingId(Integer.valueOf(id));
        buildingInfoDetailsVO.setBuildingApartmentList(buildingApartmentList);

        CustomerScan customerScan = new CustomerScan();
        customerScan.setBuildingInfoId(Integer.valueOf(id));
        customerScan.setLoginId(customer.getLoginId());
        customerScan.setScanDate(new Date());
        customerScanMapper.insert(customerScan);

        return buildingInfoDetailsVO;
    }

    @Override
    public List<BuildingInfoAndApartmentVO> selectInfoAndApart(Map paraMap) {
        return buildingInfoMapper.selectInfoAndApart(paraMap);
    }

    @Override
    public BuildingInfo selectBuildingInfoMore(String id) {
        BuildingInfo buildingInfo = buildingInfoMapper.selectByBuildingId(Integer.valueOf(id));

        return buildingInfo;
    }

    @Override
    public BuildingInfoAndRoleVO selectBuildAndRole() {
        Customer customer = (Customer) SecurityUtils.getSubject().getPrincipal();
        if (customer == null) {
            return null;
        }
        BuildingInfoAndRoleVO buildingInfoAndRoleVO = new BuildingInfoAndRoleVO();
        List<BuildingInfo> buildingInfos = buildingInfoMapper.selectList(
                new EntityWrapper<BuildingInfo>().eq("del_flag", 0)
        );
        List<AppRole> roles = appRoleMapper.selectList(
                new EntityWrapper<AppRole>().in("id", "2,5")
        );
        Integer customerId = customerMapper.selectById(customer.getId()).getRoleId();
        Integer buildingInfoId = buildingInfoMapper.buildingInfoIdByCustomerId(customer.getId());
        buildingInfoAndRoleVO.setBuildingInfoList(buildingInfos);
        buildingInfoAndRoleVO.setRoleList(roles);
        buildingInfoAndRoleVO.setNowBuildingInfoId(buildingInfoId);
        buildingInfoAndRoleVO.setNowRoleId(customerId);
        return buildingInfoAndRoleVO;
    }
}
