package com.fnzn.fnxf.app.service.user;

import com.fnzn.fnxf.app.entity.user.UserContacts;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户通讯录记录表 服务类
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
public interface UserContactsService extends IService<UserContacts> {
	public boolean recordUserContacts(String contacts,String ipAddress,Integer userId);
}
