package com.fnzn.fnxf.app.service.impl.voucher;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.voucher.VoucherApplication;
import com.fnzn.fnxf.app.mapper.voucher.VoucherApplicationMapper;
import com.fnzn.fnxf.app.service.voucher.IVoucherApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 代金券申请表 服务实现类
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@Service
public class VoucherApplicationServiceImpl extends ServiceImpl<VoucherApplicationMapper, VoucherApplication> implements IVoucherApplicationService {

    @Autowired
    private VoucherApplicationMapper voucherApplicationMapper;

    /**
     *
     * @param status
     * @param loginId
     * @return
     */
    @Override
    public List<VoucherApplication> getVoucherApplicationToCheck(String status, String loginId) {
        List<VoucherApplication> voucherApplications = voucherApplicationMapper.getVoucherApplicationToCheck(status, loginId);
        return voucherApplications;
    }

    /**
     *
     * @param pass
     * @param fail
     * @param loginId
     * @return
     */
    @Override
    public List<VoucherApplication> getVoucherApplicationChecked(String pass, String fail, String loginId) {
        List<VoucherApplication> voucherApplications = voucherApplicationMapper.getVoucherApplicationChecked(pass, fail, loginId);
        return voucherApplications;
    }
	
}
