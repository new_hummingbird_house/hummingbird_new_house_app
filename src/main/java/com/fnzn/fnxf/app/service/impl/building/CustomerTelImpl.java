package com.fnzn.fnxf.app.service.impl.building;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.customer.CustomerTel;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.building.CustomerTelMapper;
import com.fnzn.fnxf.app.service.building.CustomerTelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author Administrator
 */
@Service
public class CustomerTelImpl  extends ServiceImpl<CustomerTelMapper,CustomerTel>
        implements CustomerTelService {

    @Autowired
    private CustomerTelMapper customerTelMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertTelHistory(String phone,Customer customer) {

        CustomerTel customerTel = new CustomerTel();
        customerTel.setLoginId(customer.getLoginId());
        customerTel.setPhoneTel(phone);
        customerTel.setRealName(customer.getRealName());
        customerTel.setTelDate(new Date());

        customerTelMapper.insert(customerTel);
    }
}
