package com.fnzn.fnxf.app.service.impl.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.bean.consumer.ReportDictVO;
import com.fnzn.fnxf.app.bean.consumer.StaffReportVO;
import com.fnzn.fnxf.app.entity.adviser.BuildingInfoAdviser;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import com.fnzn.fnxf.app.entity.core.CoreDict;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.adviser.BuildingInfoAdviserMapper;
import com.fnzn.fnxf.app.mapper.building.BuildingAdviserMapper;
import com.fnzn.fnxf.app.mapper.building.BuildingInfoMapper;
import com.fnzn.fnxf.app.mapper.consumer.StaffReportMapper;
import com.fnzn.fnxf.app.mapper.core.CoreDictMapper;
import com.fnzn.fnxf.app.service.consumer.StaffReportService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional(rollbackFor = Exception.class)
@Service
public class StaffReportServiceImpl extends ServiceImpl<StaffReportMapper, StaffReport>
        implements StaffReportService {
    @Autowired
    private BuildingInfoMapper buildingInfoMapper;
    @Autowired
    private CoreDictMapper coreDictMapper;
    @Autowired
    private StaffReportMapper staffReportMapper;
    @Autowired
    private BuildingAdviserMapper adviserMapper;
    @Autowired
    private BuildingInfoAdviserMapper buildingAdviserMapper;


    @Override
    public List<StaffReportVO> findStaffReportVO(Integer userId) {
        return staffReportMapper.findStaffReportVO(userId);
    }

    @Override
    public ReportDictVO findReportDict(){
        List<BuildingInfo> buildingInfoList = buildingInfoMapper.selectList(
                new EntityWrapper<BuildingInfo>().eq("del_flag", "0")
        );
        List<CoreDict> coreDictList = coreDictMapper.selectList(
                new EntityWrapper<CoreDict>()
                        .eq("del_flag", "0")
                        .eq("TYPE","demand")
        );
        return new ReportDictVO(buildingInfoList, coreDictList);
    }

    @Override
    public Map<String,Object> findAdviserOneByBuildingId(Integer buildingId){
        Subject subject = SecurityUtils.getSubject();
        Customer customer = (Customer)subject.getPrincipal();
        BuildingInfoAdviser buildingInfoAdviser = new BuildingInfoAdviser();
        buildingInfoAdviser.setBuildingInfoId(buildingId.toString());
        buildingInfoAdviser.setNowAdviser(1);
        buildingInfoAdviser = buildingAdviserMapper.selectOne(buildingInfoAdviser);

        List<BuildingAdviser> list = new ArrayList<>();
        Map map = new HashMap();
        if(subject.hasRole("zuoXiao")||subject.hasRole("xingXiao")){
            BuildingAdviser buildingAdviser = adviserMapper.selectMeByBuilding(buildingId,customer.getId());
            if(buildingAdviser!=null){
                list.add(buildingAdviser);
            }
            map.put("adviserCaptain",list);
            List<BuildingAdviser> buildingAdvisers = adviserMapper.selectNoMeByBuilding(buildingId, customer.getId());
            map.put("adviserMember",buildingAdvisers);
//            List<BuildingAdviser> buildingAdvisers = adviserMapper.selectAdviserListByBuildingId(buildingId);
            return  map;
        }else{

            /**如果不存在下一位顾问，查找第一个**/
            if(buildingInfoAdviser==null){
                list.add(adviserMapper.findAdviserOneByBuildingId(buildingId));
                map.put("adviserCaptain",list);
                return map;
            }
            /**如果存在下一位，查询当前顾问**/
            else{
                BuildingAdviser buildingAdviser = new BuildingAdviser();
                buildingAdviser.setId(Integer.valueOf(buildingInfoAdviser.getAdviserId()));
                list.add(adviserMapper.selectOne(buildingAdviser));
                map.put("adviserCaptain",list);
                return map;
            }
        }
    }

    @Override
    public boolean adviserRestartSort(Integer adviserId,Integer buildingInfoId){
        BuildingInfoAdviser buildingInfoAdviser =  new BuildingInfoAdviser();

        /**重置顺序**/
        buildingInfoAdviser.setNowAdviser(0);
        Integer one = buildingAdviserMapper.update(buildingInfoAdviser,
                new EntityWrapper<BuildingInfoAdviser>()
                        .eq("now_adviser", "1")
                        .eq("building_info_id", buildingInfoId)
        );
        /**当前顾问轮到下一次*/
        buildingInfoAdviser = buildingAdviserMapper
                .selectNextAdviser(adviserId, buildingInfoId);
        /**不过不存在下一个顾问，就从头开始**/
        if(buildingInfoAdviser==null){
            buildingInfoAdviser = new BuildingInfoAdviser();
            buildingInfoAdviser.setBuildingInfoId(buildingInfoId.toString());
            buildingInfoAdviser = buildingAdviserMapper.selectList(
                    new EntityWrapper<BuildingInfoAdviser>()
                            .eq("building_info_id", buildingInfoId)
            ).get(0);
            buildingInfoAdviser.setNowAdviser(1);
            buildingAdviserMapper.updateById(buildingInfoAdviser);
        }else{
            buildingInfoAdviser.setNowAdviser(1);
            buildingAdviserMapper.updateById(buildingInfoAdviser);
        }
        return true;
    }
}
