package com.fnzn.fnxf.app.service.impl.browsingHistory;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.browsingHistory.BrowsingHistory;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.mapper.browsingHistory.BrowsingHistoryMapper;
import com.fnzn.fnxf.app.service.browsingHistory.IBrowsingHistoryService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 浏览信息控制表 服务实现类
 * </p>
 *
 * @author xiaomh
 * @since 2018-09-03
 */
@Service
public class BrowsingHistoryServiceImpl extends ServiceImpl<BrowsingHistoryMapper, BrowsingHistory> implements IBrowsingHistoryService {

    @Autowired
    private BrowsingHistoryMapper browsingHistoryMapper;

    @Override
    public  List<BrowsingHistory> checkBrowsingHistory(String id) {
        String jsessionId = SecurityUtils.getSubject().getSession().getId().toString();

        EntityWrapper<BrowsingHistory> entityWrapper = new EntityWrapper<BrowsingHistory>();
        entityWrapper.eq("jsession_id" , jsessionId);
        entityWrapper.eq("type" , id);

        List<BrowsingHistory> browsingHistoriesList = browsingHistoryMapper.selectList(entityWrapper);

        return browsingHistoriesList;
    }


}
