package com.fnzn.fnxf.app.service.impl.customer;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.bean.core.RolePermissionVO;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.filter.MyShiroRealm;
import com.fnzn.fnxf.app.mapper.adviser.BuildingInfoAdviserMapper;
import com.fnzn.fnxf.app.mapper.core.AppPermissionMapper;
import com.fnzn.fnxf.app.mapper.core.AppRoleMapper;
import com.fnzn.fnxf.app.mapper.customer.CustomerMapper;
import com.fnzn.fnxf.app.service.customer.CustomerService;
import com.fnzn.fnxf.app.utils.Constants;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerSeriveImpl extends ServiceImpl<CustomerMapper,Customer> implements CustomerService {
    @Autowired
    private AppRoleMapper appRoleMapper;
    @Autowired
    private AppPermissionMapper appPermissionMapper;
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private PybUtils pybUtils;
    @Value("${imageUrl.customer.headImage}")
    private String headImage;
    @Autowired
    private BuildingInfoAdviserMapper buildingInfoAdviserMapper;

    /**
     * 查询用户的角色和权限
     * @return
     */
    @Override
    public RolePermissionVO findCustomerRolAndPer(Customer customer){
        Set<String> appRoles = appRoleMapper.selectByUserId(customer.getId());
        Set<String> appPermissions = appPermissionMapper.selectByUserId(customer.getId());
        Customer newCustomer = customerMapper.selectById(customer.getId());
        newCustomer.setType(customer.getType());
        newCustomer.setFirst(customer.isFirst());
        return new RolePermissionVO(newCustomer,appRoles,appPermissions);
    }

    @Override
    public boolean updateCustomerInfo(Customer newCustomer,Integer buildingInfoId) {
        Customer oldCustomer = customerMapper.selectById(newCustomer.getId());
        String oldPhotoUrl = oldCustomer.getPhotoUrl();
        String newPhotoUrl = newCustomer.getPhotoUrl();
        if(PybUtils.objectIsNotEmpty(newPhotoUrl)){
            if(PybUtils.objectIsNotEmpty(oldPhotoUrl)&&!oldPhotoUrl.equals(Constants.CUSTOMERPIC)){
                pybUtils.delFile(oldPhotoUrl);
            }
            String nowPhotoUrl = pybUtils.GenerateImage(newPhotoUrl, headImage);
            newCustomer.setPhotoUrl(nowPhotoUrl);
        }else{
            newCustomer.setPhotoUrl(null);
        }
        List<Integer> roles = Arrays.asList(2, 5);
        if(roles.contains(oldCustomer.getRoleId())){
            if(PybUtils.objectIsNotEmpty(buildingInfoId)){
                boolean isSuccess = buildingInfoAdviserMapper.updateBuildingInfoIdByCustomerId(
                        oldCustomer.getId(),buildingInfoId);
                if(!isSuccess){
                    throw  new RuntimeException("更新所属楼盘出错");
                }
            }
        }else{
            newCustomer.setRoleId(null);
        }
        boolean isSuccess = customerMapper.updateById(newCustomer)>0;
        MyShiroRealm.clearAuth();
        return isSuccess;
    }
}
