package com.fnzn.fnxf.app.service.mall;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.mall.MallInfoVO;

/**
 * @author Administrator
 */
public interface MallInfoVOService extends IService<MallInfoVO> {
    /**
     * 商品详情
     * @param id
     * @return
     */
    MallInfoVO selectMallInfoDetailById(String id);
}

