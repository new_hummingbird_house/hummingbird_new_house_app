package com.fnzn.fnxf.app.service.impl.mall;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.browsingHistory.BrowsingHistory;
import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.mall.MallInfoDetail;
import com.fnzn.fnxf.app.entity.mall.MallInfoPics;
import com.fnzn.fnxf.app.bean.mall.MallInfoVO;
import com.fnzn.fnxf.app.mapper.mall.MallInfoDetailMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoPicsMapper;
import com.fnzn.fnxf.app.mapper.mall.MallInfoVOMapper;
import com.fnzn.fnxf.app.service.browsingHistory.IBrowsingHistoryService;
import com.fnzn.fnxf.app.service.mall.MallInfoVOService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Administrator
 */
@Service
public class MallInfoVOServiceImpl extends ServiceImpl<MallInfoVOMapper, MallInfoVO> implements MallInfoVOService {

    @Autowired
    private MallInfoDetailMapper mallInfoDetailMapper;

    @Autowired
    private MallInfoMapper mallInfoMapper;

    @Autowired
    private MallInfoPicsMapper mallInfoPicsMapper;

    @Autowired
    private IBrowsingHistoryService browsingHistoryService;

    @Override
    public MallInfoVO selectMallInfoDetailById(String id) {
        String jsessionId = SecurityUtils.getSubject().getSession().getId().toString();
        Customer customer = (Customer)SecurityUtils.getSubject().getPrincipal();
        BrowsingHistory browsingHistory = new BrowsingHistory();
        browsingHistory.setJsessionId(jsessionId);
        browsingHistory.setType("1");
        browsingHistory.setLoginId(customer.getLoginId());
        browsingHistory.setBrowsingId(id);
        browsingHistoryService.insert(browsingHistory);

        MallInfoVO mallInfoVO = new MallInfoVO();

        MallInfo mallInfo = mallInfoMapper.selectById(Integer.valueOf(id));

        Map<String,Object> map = new HashMap<String,Object>(2);
        map.put("mall_info_id", Integer.valueOf(id));

        List<MallInfoDetail> mallInfoDetailList = mallInfoDetailMapper.selectByMap(map);

        Collections.sort(mallInfoDetailList, new Comparator<MallInfoDetail>() {
            @Override
            public int compare(MallInfoDetail o1, MallInfoDetail o2) {
                // 按照图片的顺序进行升序排列
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                }
                if (o1.getOrder().equals(o2.getOrder())) {
                    return 0;
                }
                return -1;
            }
        });

        List<MallInfoPics> mallInfoPicsList = mallInfoPicsMapper.selectByMap(map);

        mallInfoVO.setMallInfo(mallInfo);
        mallInfoVO.setMallInfoDetailList(mallInfoDetailList);
        mallInfoVO.setMallInfoPicsList(mallInfoPicsList);

        return mallInfoVO;
    }
}
