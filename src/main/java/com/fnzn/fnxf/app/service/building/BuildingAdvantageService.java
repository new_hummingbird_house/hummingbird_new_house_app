package com.fnzn.fnxf.app.service.building;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.building.BuildingAdvantage;

public interface BuildingAdvantageService extends IService<BuildingAdvantage> {
}
