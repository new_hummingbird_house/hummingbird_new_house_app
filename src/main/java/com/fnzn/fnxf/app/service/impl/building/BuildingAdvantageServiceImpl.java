package com.fnzn.fnxf.app.service.impl.building;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fnzn.fnxf.app.entity.building.BuildingAdvantage;
import com.fnzn.fnxf.app.mapper.building.BuildingAdvantageMapper;
import com.fnzn.fnxf.app.service.building.BuildingAdvantageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = Exception.class)
@Service
public class BuildingAdvantageServiceImpl extends ServiceImpl<BuildingAdvantageMapper, BuildingAdvantage>
        implements BuildingAdvantageService {
}
