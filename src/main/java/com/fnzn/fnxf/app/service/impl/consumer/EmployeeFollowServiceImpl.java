package com.fnzn.fnxf.app.service.impl.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import com.fnzn.fnxf.app.entity.core.CoreDict;
import com.fnzn.fnxf.app.entity.logs.OperateLog;
import com.fnzn.fnxf.app.mapper.building.BuildingAdviserMapper;
import com.fnzn.fnxf.app.mapper.consumer.ConsumerCommendMapper;
import com.fnzn.fnxf.app.mapper.consumer.StaffReportMapper;
import com.fnzn.fnxf.app.mapper.core.CoreDictMapper;
import com.fnzn.fnxf.app.mapper.logs.OperateLogMapper;
import com.fnzn.fnxf.app.service.consumer.EmployeeFollowService;
import com.fnzn.fnxf.app.utils.PybUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeFollowServiceImpl implements EmployeeFollowService {
    @Autowired
    private BuildingAdviserMapper adviserMapper;
    @Autowired
    private CoreDictMapper coreDictMapper;
    @Autowired
    private ConsumerCommendMapper consumerCommendMapper;
    @Autowired
    private StaffReportMapper staffReportMapper;
    @Autowired
    private OperateLogMapper operateLogMapper;

    @Override
    public List<Map<String,Object>> findMyCustomer(String phone){
        List<Map<String,Object>> list = new ArrayList<>();
        BuildingAdviser adviser = new BuildingAdviser();
        adviser.setPhone(phone);
        adviser = adviserMapper.selectOne(adviser);
        if(adviser==null){
            return list;
        }
        List<Map<String, Object>> commendMap = consumerCommendMapper.findCommendByAdviserId(2,adviser.getId());
        List<Map<String, Object>> reportMap = staffReportMapper.findReportByAdviserId(1,adviser.getId());
        list.addAll(commendMap);
        list.addAll(reportMap);
        return list;
    }

    @Override
    public Map<String,Object> getMyCustomerById(Integer sourceType,Integer id,String phone){
        Map<String, Object> customerMap = null;
        BuildingAdviser adviser = new BuildingAdviser();
        adviser.setPhone(phone);
        adviser = adviserMapper.selectOne(adviser);
        if(adviser==null) {
            return null;
        }
        //报备
        if(sourceType==1){
            customerMap = staffReportMapper.getMyCustomerById(id,adviser.getId());
            customerMap.put("sourceType",sourceType);
            Integer progressStatus = Integer.valueOf(customerMap.get("progressStatus").toString());
            List<Map<String, Object>> nextStatus = coreDictMapper.findNextProgressStatus(progressStatus);
            /**成员列表**/
            List<String> list = null;
            StaffReport staffReport = staffReportMapper.selectById(id);
            String adviserMember = "";
            if(staffReport!=null){
                adviserMember = staffReport.getMemberIds();
            }
            /**分割为集合**/
            if(PybUtils.objectIsNotEmpty(adviserMember)){
                String[] adviserMemberArr = adviserMember.split(",");
                list =staffReportMapper.selectAdviserMemberList(adviserMemberArr);
            }
            customerMap.put("nextStatus",nextStatus);
            customerMap.put("adviserMember",list);
        }
        //推荐
        else if(sourceType==2){
            customerMap = consumerCommendMapper.getMyCustomerById(id,adviser.getId());
            customerMap.put("sourceType",sourceType);
            Integer progressStatus = Integer.valueOf(customerMap.get("progressStatus").toString());
            List<Map<String, Object>> nextStatus = coreDictMapper.findNextProgressStatus(progressStatus);
            customerMap.put("nextStatus",nextStatus);
        }
        else{
            return new HashMap<>();
        }
        return customerMap;
    }

    @Override
    public boolean updateProgressStatus(OperateLog operateLog, Integer sourceType, Integer sourceId,String phone){
        BuildingAdviser adviser = new BuildingAdviser();
        adviser.setPhone(phone);
        adviser = adviserMapper.selectOne(adviser);
        if(adviser==null){
            return false;
        }
        if(sourceType==1){
            StaffReport staffReport = new StaffReport();
            //报备记录ID
            staffReport.setId(sourceId);
            //报备人是否归该置业顾问管理
            staffReport.setAdviserId(adviser.getId());
            staffReport = staffReportMapper.selectOne(staffReport);
            //设置操作信息的原状态
            operateLog.setOldStatus(staffReport.getProgressStatus());
            //设置需要更新的状态
            staffReport.setProgressStatus(operateLog.getUpdateStatus());
            Integer successCount = staffReportMapper.updateById(staffReport);
            //成功后记录日志
            if(staffReport!=null&&successCount>0){
                //设置操作人的登陆名
                operateLog.setLoginId(phone);
                //设置操作的表名
                operateLog.setTableName("staff_report");
                //设置操作的记录ID
                operateLog.setRecordId(staffReport.getId());
                //保存日志
                successCount = operateLogMapper.insert(operateLog);
            }
            return successCount>0;
        }
        //推荐
        else if(sourceType==2){
            ConsumerCommend consumerCommend = new ConsumerCommend();
            //推荐记录ID
            consumerCommend.setId(sourceId);
            //推荐人是否归该置业顾问管理
            consumerCommend.setAdviserId(adviser.getId());
            consumerCommend = consumerCommendMapper.selectOne(consumerCommend);
            //设置操作信息的原状态
            operateLog.setOldStatus(consumerCommend.getProgressStatus());
            //设置需要更新的状态
            consumerCommend.setProgressStatus(operateLog.getUpdateStatus());
            Integer successCount = consumerCommendMapper.updateById(consumerCommend);
            //成功后记录日志
            if(consumerCommend!=null&&successCount>0){
                //设置操作人的登陆名
                operateLog.setLoginId(phone);
                //设置操作的表名
                operateLog.setTableName("consumer_commend");
                //设置操作的记录ID
                operateLog.setRecordId(consumerCommend.getId());
                //保存日志
                successCount = operateLogMapper.insert(operateLog);
            }
            return successCount>0;
        }else{
            return false;
        }
    }

    @Override
    public boolean isSamePhone(String phone,String sparePhone,Integer buildingInfoId){
        List<String> phoneList = new ArrayList<>();
        if(PybUtils.objectIsNotEmpty(phone)){
            phoneList.add(phone);
        }
        if(PybUtils.objectIsNotEmpty(sparePhone)){
            phoneList.add(sparePhone);
        }
        String inStr = StringUtils.join(phoneList,",");
        //报备中没有重名号码
        List<StaffReport> staffReports = staffReportMapper.selectList(
                new EntityWrapper<StaffReport>()
                        .ne("progress_status", "0")
                        .in("phone", inStr)
                        .eq("building_info_id", buildingInfoId)
                        .or("spare_phone in ("+inStr+")")

        );
        if(staffReports!=null&&staffReports.size()>0){
            return true;
        }
        //推荐中没有重名号码

        List<ConsumerCommend> consumerCommends = consumerCommendMapper.selectList(
                new EntityWrapper<ConsumerCommend>()
                        .ne("progress_status", "0")
                        .eq("building_info_id", buildingInfoId)
                        .in("phone",inStr)
                        .or("spare_phone in ("+inStr+")")
        );
        if(consumerCommends!=null&&consumerCommends.size()>0){
            return true;
        }
        return false;
    }

    /**
     * 字典查询
     * @return
     */
    public List<CoreDict> findCustomerDict(){
        List<CoreDict> coreDicts = coreDictMapper.selectList(
                new EntityWrapper<CoreDict>()
                        .eq("DEL_FLAG", "0")
                        .eq("TYPE", "progress_status")
        );
        return coreDicts;
    }
}
