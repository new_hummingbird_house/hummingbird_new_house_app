package com.fnzn.fnxf.app.service.building;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.building.Information;

/**
 * 新房资讯Service
 */
public interface InformationService extends IService<Information> {
    public Information selectAndUpdateReadNum(Integer id);
}
