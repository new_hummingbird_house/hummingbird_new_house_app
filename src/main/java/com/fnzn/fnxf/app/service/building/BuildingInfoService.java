package com.fnzn.fnxf.app.service.building;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.bean.building.BuildingInfoAndRoleVO;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.bean.building.BuildingInfoAndApartmentVO;

import java.util.List;
import java.util.Map;

import com.fnzn.fnxf.app.bean.building.BuildingInfoDetailsVO;
import com.fnzn.fnxf.app.entity.customer.Customer;

/**
 * @author Administrator
 */
public interface BuildingInfoService extends IService<BuildingInfo>{

    /**
     * @param id
     * @param customer
     * @return
     */
    BuildingInfoDetailsVO buildingDetails(String id, Customer customer);

    /**
     * @param paraMap
     * @return
     */
    List<BuildingInfoAndApartmentVO> selectInfoAndApart(Map paraMap);

    /**
     * @param id
     * @return
     */
    BuildingInfo selectBuildingInfoMore(String id);

    /**
     * 查询所属楼盘和角色
     * @return
     */
    BuildingInfoAndRoleVO selectBuildAndRole();
}
