package com.fnzn.fnxf.app.service.building;

import com.baomidou.mybatisplus.service.IService;
import com.fnzn.fnxf.app.entity.building.BuildingApartment;

import java.util.List;

public interface BuildingApartmentService extends IService<BuildingApartment> {

    List<BuildingApartment> selectApartmentByBuildingId(String id);
}
