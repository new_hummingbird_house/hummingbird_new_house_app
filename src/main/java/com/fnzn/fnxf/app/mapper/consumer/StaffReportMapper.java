package com.fnzn.fnxf.app.mapper.consumer;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.bean.consumer.StaffReportVO;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface StaffReportMapper extends BaseMapper<StaffReport> {
    /**根据用户ID，查询自己的报备信息**/
    public List<StaffReportVO> findStaffReportVO(Integer userId);
    /**根据顾问ID查询报备列表**/
    public List<Map<String,Object>> findReportByAdviserId(@Param(value = "sourceType") Integer sourceType,
                                                          @Param(value = "id") Integer id);
    /**根据顾问ID和报备ID查询单条数据**/
    public Map<String,Object> getMyCustomerById(@Param(value = "id") Integer id,
                                                @Param(value = "adviserId") Integer adviserId);
    /**修改指定天数内，初始状态无变化的记录**/
    public boolean updateInvalidOnThreeDay(@Param(value = "days")Integer days);

    /**修改指定天数内，带看后无变化的记录**/
    public boolean updateFollowOnDay(@Param(value = "days")Integer days);

    /**获取报备成员的信息**/
    List<String> selectAdviserMemberList(String[] adviserArr);

}
