package com.fnzn.fnxf.app.mapper.customer;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.customer.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface LoginMapper extends BaseMapper<Customer> {

    List<Customer> selectByLoginId(@Param(value = "loginId") String loginId);

    List<Customer> checkPassWord(Map<String,Object> map);
}
