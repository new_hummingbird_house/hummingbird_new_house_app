package com.fnzn.fnxf.app.mapper.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.user.UserContacts;

/**
 * <p>
  * 用户通讯录记录表 Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
public interface UserContactsMapper extends BaseMapper<UserContacts> {

}