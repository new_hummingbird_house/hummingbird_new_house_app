package com.fnzn.fnxf.app.mapper.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.user.UserDemand;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-26
 */
public interface UserDemandMapper extends BaseMapper<UserDemand> {
    List<String> selectAdvantageListInType(String[] advantage);
    List<String> selectBuildingAreaList(String[] buildingArea);
}