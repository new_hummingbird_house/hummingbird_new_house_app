package com.fnzn.fnxf.app.mapper.voucher;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 代金券信息表 Mapper 接口
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@Repository
public interface VoucherInfoMapper extends BaseMapper<VoucherInfo> {

    /**
     * @param loginId
     * @param status
     * @return
     */
    List<VoucherInfoVO> selectVoucherInfoByStatus(@Param(value = "loginId")String loginId, @Param(value = "status")String status);

    /**
     * @param loginId
     */
    void updateByDueTime(@Param(value = "loginId")String loginId, @Param(value = "dutDate")String dutDate);

    /**
     * @param id
     * @return
     */
    VoucherInfoVO selectVoucherDetailById(@Param(value = "id") Integer id);

    /**
     * 插入数据(返回值为主键ID)
     * @param voucherInfo
     * @return
     */
    Integer add(VoucherInfo voucherInfo);


    List<VoucherInfoVO> selectVoucherDetailByCustomer(Map map);
}