package com.fnzn.fnxf.app.mapper.mall;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.bean.mall.MallInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface MallInfoVOMapper extends BaseMapper<MallInfoVO> {
}
