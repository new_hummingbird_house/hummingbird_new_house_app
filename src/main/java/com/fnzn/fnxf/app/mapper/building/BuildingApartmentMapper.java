package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.building.BuildingApartment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 楼房户型
 */
@Mapper
@Repository
public interface BuildingApartmentMapper extends BaseMapper<BuildingApartment> {

    List<BuildingApartment> selectListByBuildingId(Integer integer);
}
