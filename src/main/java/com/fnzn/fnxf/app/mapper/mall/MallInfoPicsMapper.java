package com.fnzn.fnxf.app.mapper.mall;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.mall.MallInfoPics;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface MallInfoPicsMapper extends BaseMapper<MallInfoPics> {
}
