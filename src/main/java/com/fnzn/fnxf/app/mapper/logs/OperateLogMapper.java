package com.fnzn.fnxf.app.mapper.logs;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.logs.OperateLog;

/**
 * <p>
  * 员工操作日志 Mapper 接口
 * </p>
 *
 * @author xiaomh
 * @since 2018-08-03
 */
public interface OperateLogMapper extends BaseMapper<OperateLog> {

}