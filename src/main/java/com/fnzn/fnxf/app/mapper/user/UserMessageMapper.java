package com.fnzn.fnxf.app.mapper.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.user.UserMessage;

/**
 * <p>
  * 记录用户短信 Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-25
 */
public interface UserMessageMapper extends BaseMapper<UserMessage> {

}