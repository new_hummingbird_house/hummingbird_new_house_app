package com.fnzn.fnxf.app.mapper.adviser;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.adviser.BuildingInfoAdviser;
import org.apache.ibatis.annotations.Param;

public interface BuildingInfoAdviserMapper extends BaseMapper<BuildingInfoAdviser> {

    public BuildingInfoAdviser selectNextAdviser(@Param("adviserId")Integer adviserId,
                                                 @Param("buildingInfoId")Integer buildingInfoId);
    public boolean updateBuildingInfoIdByCustomerId(@Param("customerId") Integer customerId,
                                                    @Param("buildingInfoId") Integer buildingInfoId);
}
