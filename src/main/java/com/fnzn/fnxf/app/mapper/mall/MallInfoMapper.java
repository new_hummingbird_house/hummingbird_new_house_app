package com.fnzn.fnxf.app.mapper.mall;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.mall.MallInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface MallInfoMapper extends BaseMapper<MallInfo> {

    /**
     * @param map
     * @return
     */
    List<MallInfo> selectBySearch(Map map);
}
