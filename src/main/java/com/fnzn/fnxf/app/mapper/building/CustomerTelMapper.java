package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.customer.CustomerTel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface CustomerTelMapper extends BaseMapper<CustomerTel> {
}
