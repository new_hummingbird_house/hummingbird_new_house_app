package com.fnzn.fnxf.app.mapper.browsingHistory;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.browsingHistory.BrowsingHistory;

/**
 * <p>
  * 浏览信息控制表 Mapper 接口
 * </p>
 *
 * @author xiaomh
 * @since 2018-09-03
 */
public interface BrowsingHistoryMapper extends BaseMapper<BrowsingHistory> {

}