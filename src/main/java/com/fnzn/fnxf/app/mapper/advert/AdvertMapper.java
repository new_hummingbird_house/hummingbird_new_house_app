package com.fnzn.fnxf.app.mapper.advert;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.advert.Advert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface AdvertMapper extends BaseMapper<Advert> {
}
