package com.fnzn.fnxf.app.mapper.customer;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.customer.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CustomerMapper extends BaseMapper<Customer> {
    Customer selectCustomer();
}
