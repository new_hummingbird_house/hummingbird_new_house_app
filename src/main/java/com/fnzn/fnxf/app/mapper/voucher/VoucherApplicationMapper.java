package com.fnzn.fnxf.app.mapper.voucher;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.bean.voucher.VoucherVO;
import com.fnzn.fnxf.app.entity.voucher.VoucherApplication;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
  * 代金券申请表 Mapper 接口
 * </p>
 *
 * @author JokerGao
 * @since 2018-07-20
 */
@Repository
public interface VoucherApplicationMapper extends BaseMapper<VoucherApplication> {

    VoucherVO getVoucherVO(@Param(value = "buildingInfoId")Integer buildingInfoId, @Param(value = "customerId")Integer customerId);

    List<VoucherApplication> getVoucherApplicationToCheck(@Param(value = "status")String status, @Param(value = "loginId")String loginId);

    List<VoucherApplication> getVoucherApplicationChecked(@Param(value = "pass")String pass, @Param(value = "fail")String fail, @Param(value = "loginId")String loginId);

}