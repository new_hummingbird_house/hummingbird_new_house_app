package com.fnzn.fnxf.app.mapper.core;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.core.AppPermission;
import java.util.Set;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-23
 */
public interface AppPermissionMapper extends BaseMapper<AppPermission> {
    /**根据用户ID查询权限**/
    public Set<String> selectByUserId(Integer userId);
}