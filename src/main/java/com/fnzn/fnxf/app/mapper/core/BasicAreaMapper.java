package com.fnzn.fnxf.app.mapper.core;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.core.BasicArea;

/**
 * <p>
  * BASIC_地区表 Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-27
 */
public interface BasicAreaMapper extends BaseMapper<BasicArea> {

}