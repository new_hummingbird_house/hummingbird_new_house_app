package com.fnzn.fnxf.app.mapper.core;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.core.CoreDict;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CoreDictMapper extends BaseMapper<CoreDict> {
    public List<Map<String,Object>> findNextProgressStatus(@Param(value = "status") Integer status);
}
