package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.bean.building.BuildingInfoAndApartmentVO;
import com.fnzn.fnxf.app.bean.building.BuildingInfoDetailsVO;
import com.fnzn.fnxf.app.entity.building.BuildingPhoto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 楼房基本信息
 */
@Mapper
@Repository
public interface BuildingInfoMapper extends BaseMapper<BuildingInfo> {
    /**
     * 查询楼盘以及特色
     * @return
     */
    public List<BuildingInfoAndApartmentVO> selectInfoAndApart(Map paraMap);

    BuildingInfoDetailsVO BuildingInfoDetailsById(@Param(value = "id") Integer id);

    List<BuildingPhoto> selectBuildingPhotoByBuildingId(Integer integer);

    BuildingInfo selectByBuildingId(Integer integer);

    Integer buildingInfoIdByCustomerId(@Param(value = "id") Integer id);
}
