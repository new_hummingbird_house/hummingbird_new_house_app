package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BuildingAdviserMapper extends BaseMapper<BuildingAdviser> {

    List<BuildingAdviser> selectAdviserListByBuildingId(Integer integer);

    BuildingAdviser findAdviserOneByBuildingId(Integer buildingId);

    /**根据所选楼盘查询自己的顾问信息**/
    BuildingAdviser selectMeByBuilding(@Param(value = "buildingInfoId") Integer buildingInfoId,
                                             @Param(value = "customerId") Integer customerId);
    /**根据所选楼盘查询非自己的顾问信息*/
    List<BuildingAdviser> selectNoMeByBuilding(@Param(value = "buildingInfoId") Integer buildingInfoId,
                                             @Param(value = "customerId") Integer customerId);
}
