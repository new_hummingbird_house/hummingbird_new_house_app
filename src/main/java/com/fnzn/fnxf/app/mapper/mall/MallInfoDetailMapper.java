package com.fnzn.fnxf.app.mapper.mall;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.mall.MallInfoDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 */
@Mapper
@Repository
public interface MallInfoDetailMapper extends BaseMapper<MallInfoDetail> {

}
