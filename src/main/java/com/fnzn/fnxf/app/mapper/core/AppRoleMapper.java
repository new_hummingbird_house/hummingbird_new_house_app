package com.fnzn.fnxf.app.mapper.core;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.core.AppRole;
import java.util.Set;

/**
 * <p>
  * APP角色 Mapper 接口
 * </p>
 *
 * @author pyb
 * @since 2018-07-23
 */
public interface AppRoleMapper extends BaseMapper<AppRole> {
    /**根据用户ID查询角色**/
    public Set<String> selectByUserId(Integer userId);
}