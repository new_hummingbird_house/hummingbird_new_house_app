package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.building.Information;
import org.springframework.stereotype.Repository;

/**
 * 新房资讯
 */
@Repository
public interface InformationMapper extends BaseMapper<Information> {

}
