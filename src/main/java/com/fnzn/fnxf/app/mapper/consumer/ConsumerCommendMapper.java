package com.fnzn.fnxf.app.mapper.consumer;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.bean.consumer.ConsumerComVO;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ConsumerCommendMapper extends BaseMapper<ConsumerCommend> {
    public List<ConsumerComVO> findConsumerComVO(Integer userId);
    public List<Map<String,Object>> findCommendByAdviserId(@Param(value = "sourceType") Integer sourceType,
                                                           @Param(value = "id") Integer id);
    public Map<String,Object> getMyCustomerById(@Param(value = "id") Integer id,
                                                @Param(value = "adviserId") Integer adviserId);
    /**修改指定天数内，初始状态无变化的记录**/
    public boolean updateInvalidOnThreeDay(@Param(value = "days")Integer days);

    /**修改指定天数内，带看后无变化的记录**/
    public boolean updateFollowOnDay(@Param(value = "days")Integer days);
}
