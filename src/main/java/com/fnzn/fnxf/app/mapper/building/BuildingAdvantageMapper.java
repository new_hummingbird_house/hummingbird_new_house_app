package com.fnzn.fnxf.app.mapper.building;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fnzn.fnxf.app.entity.building.BuildingAdvantage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 楼房特色
 */
@Mapper
@Repository
public interface BuildingAdvantageMapper extends BaseMapper<BuildingAdvantage> {
    List<BuildingAdvantage> selectAdvantageListByBuildingId(@Param(value = "id") Integer id);
}
