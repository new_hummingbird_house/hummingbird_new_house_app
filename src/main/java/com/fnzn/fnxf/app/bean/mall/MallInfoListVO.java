package com.fnzn.fnxf.app.bean.mall;

import com.fnzn.fnxf.app.entity.advert.Advert;
import com.fnzn.fnxf.app.entity.mall.MallInfo;

import java.util.List;

/**
 * @author Administrator
 */
public class MallInfoListVO {

    private List<MallInfo> mallInfoList;

    public MallInfoListVO() {
    }

    public List<MallInfo> getMallInfoList() {
        return mallInfoList;
    }

    public void setMallInfoLists(List<MallInfo> mallInfoList) {
        this.mallInfoList = mallInfoList;
    }

    @Override
    public String toString() {
        return "MallInfoListVO{" +
                "mallInfoList=" + mallInfoList +
                '}';
    }
}
