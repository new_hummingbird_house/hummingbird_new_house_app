package com.fnzn.fnxf.app.bean.consumer;

import com.fnzn.fnxf.app.entity.advert.Advert;
import com.fnzn.fnxf.app.entity.consumer.ConsumerCommend;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;

public class ConsumerComVO extends ConsumerCommend {
    private String buildingTitle;
    private String progressStatusText;
    private String adviserName;

    public String getBuildingTitle() {
        return buildingTitle;
    }

    public void setBuildingTitle(String buildingTitle) {
        this.buildingTitle = buildingTitle;
    }

    public String getProgressStatusText() {
        return progressStatusText;
    }

    public void setProgressStatusText(String progressStatusText) {
        this.progressStatusText = progressStatusText;
    }

    public String getAdviserName() {
        return adviserName;
    }

    public void setAdviserName(String adviserName) {
        this.adviserName = adviserName;
    }
}
