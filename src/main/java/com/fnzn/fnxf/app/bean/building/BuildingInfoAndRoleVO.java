package com.fnzn.fnxf.app.bean.building;

import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.entity.core.AppRole;

import java.util.List;

/**
 * @Author pyb
 */
public class BuildingInfoAndRoleVO {
    private List<BuildingInfo> buildingInfoList;
    private List<AppRole> roleList;
    private Integer nowRoleId;
    private Integer nowBuildingInfoId;

    public List<BuildingInfo> getBuildingInfoList() {
        return buildingInfoList;
    }

    public void setBuildingInfoList(List<BuildingInfo> buildingInfoList) {
        this.buildingInfoList = buildingInfoList;
    }

    public List<AppRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<AppRole> roleList) {
        this.roleList = roleList;
    }

    public Integer getNowRoleId() {
        return nowRoleId;
    }

    public void setNowRoleId(Integer nowRoleId) {
        this.nowRoleId = nowRoleId;
    }

    public Integer getNowBuildingInfoId() {
        return nowBuildingInfoId;
    }

    public void setNowBuildingInfoId(Integer nowBuildingInfoId) {
        this.nowBuildingInfoId = nowBuildingInfoId;
    }

    @Override
    public String toString() {
        return "BuldingInfoAndRoleVO{" +
                "buildingInfoList=" + buildingInfoList +
                ", roleList=" + roleList +
                ", nowRoleId=" + nowRoleId +
                ", nowBuildingInfoId=" + nowBuildingInfoId +
                '}';
    }
}
