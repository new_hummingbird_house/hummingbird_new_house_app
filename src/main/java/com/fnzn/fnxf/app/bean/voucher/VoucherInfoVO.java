package com.fnzn.fnxf.app.bean.voucher;

import com.fnzn.fnxf.app.entity.voucher.VoucherInfo;

import java.math.BigDecimal;

/**
 * @author Administrator
 */
public class VoucherInfoVO extends VoucherInfo {

    private String name;

    private BigDecimal productSalePrice;

    private String productDealer;

    private String productModel;

    private String productPayPrice;

    private String identityCard;

    public VoucherInfoVO() {
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(BigDecimal productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductDealer() {
        return productDealer;
    }

    public void setProductDealer(String productDealer) {
        this.productDealer = productDealer;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductPayPrice() {
        return productPayPrice;
    }

    public void setProductPayPrice(String productPayPrice) {
        this.productPayPrice = productPayPrice;
    }
}
