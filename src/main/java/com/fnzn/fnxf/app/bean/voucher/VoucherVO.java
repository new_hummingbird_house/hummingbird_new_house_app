package com.fnzn.fnxf.app.bean.voucher;

import com.fnzn.fnxf.app.entity.building.BuildingAdviser;

import java.util.List;

/**
 * 优惠券申请页返回信息
 */
public class VoucherVO {

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号(loginId)
     */
    private String phone;

    /**
     * 楼盘名称
     */
    private String buildingTitle;

    /**
     * 顾问
     */
    private List<BuildingAdviser> buildingAdviserList;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBuildingTitle() {
        return buildingTitle;
    }

    public void setBuildingTitle(String buildingTitle) {
        this.buildingTitle = buildingTitle;
    }

    public List<BuildingAdviser> getBuildingAdviserList() {
        return buildingAdviserList;
    }

    public void setBuildingAdviserList(List<BuildingAdviser> buildingAdviserList) {
        this.buildingAdviserList = buildingAdviserList;
    }
}
