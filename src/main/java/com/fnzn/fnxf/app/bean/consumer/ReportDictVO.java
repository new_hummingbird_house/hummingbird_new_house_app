package com.fnzn.fnxf.app.bean.consumer;

import com.fnzn.fnxf.app.entity.building.BuildingInfo;
import com.fnzn.fnxf.app.entity.core.CoreDict;

import java.util.List;

public class ReportDictVO {
    private List<BuildingInfo> buildingList;
    private List<CoreDict> commendDict;

    public ReportDictVO(List<BuildingInfo> buildingList, List<CoreDict> commendDict) {
        this.buildingList = buildingList;
        this.commendDict = commendDict;
    }

    public List<BuildingInfo> getBuildingList() {
        return buildingList;
    }

    public void setBuildingList(List<BuildingInfo> buildingList) {
        this.buildingList = buildingList;
    }

    public List<CoreDict> getCommendDict() {
        return commendDict;
    }

    public void setCommendDict(List<CoreDict> commendDict) {
        this.commendDict = commendDict;
    }
}
