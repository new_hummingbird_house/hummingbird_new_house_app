package com.fnzn.fnxf.app.bean.building;

import com.fnzn.fnxf.app.entity.customer.Customer;
import com.fnzn.fnxf.app.entity.building.*;

import java.util.List;

public class BuildingInfoDetailsVO extends BuildingInfo {

    private List<BuildingAdvantage> buildingAdvantageList;

    private List<BuildingAdviser> buildingAdviserList;

    private List<BuildingApartment> buildingApartmentList;

    private List<BuildingPhoto> buildingPhotoList;

//    private HashMap<String,String> ext2Map;

    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<BuildingAdvantage> getBuildingAdvantageList() {
        return buildingAdvantageList;
    }

    public void setBuildingAdvantageList(List<BuildingAdvantage> buildingAdvantageList) {
        this.buildingAdvantageList = buildingAdvantageList;
    }

    public List<BuildingPhoto> getBuildingPhotoList() {
        return buildingPhotoList;
    }

    public void setBuildingPhotoList(List<BuildingPhoto> buildingPhotoList) {
        this.buildingPhotoList = buildingPhotoList;
    }

    public BuildingInfoDetailsVO(){
        super();
    }

    public List<BuildingAdviser> getBuildingAdviserList() {
        return buildingAdviserList;
    }

    public void setBuildingAdviserList(List<BuildingAdviser> buildingAdviserList) {
        this.buildingAdviserList = buildingAdviserList;
    }

    public List<BuildingApartment> getBuildingApartmentList() {
        return buildingApartmentList;
    }

    public void setBuildingApartmentList(List<BuildingApartment> buildingApartmentList) {
        this.buildingApartmentList = buildingApartmentList;
    }
}
