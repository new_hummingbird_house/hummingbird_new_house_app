package com.fnzn.fnxf.app.bean.mall;

import com.fnzn.fnxf.app.bean.voucher.VoucherInfoVO;
import com.fnzn.fnxf.app.entity.mall.MallInfo;

import java.util.List;

/**
 * @author Administrator
 */
public class MallProductBuyVO {

    private MallInfo mallInfo;

    private List<VoucherInfoVO> voucherInfoVOList;

    public MallProductBuyVO() {
    }

    public List<VoucherInfoVO> getVoucherInfoVOList() {
        return voucherInfoVOList;
    }

    public void setVoucherInfoVOList(List<VoucherInfoVO> voucherInfoVOList) {
        this.voucherInfoVOList = voucherInfoVOList;
    }

    public MallInfo getMallInfo() {
        return mallInfo;
    }

    public void setMallInfo(MallInfo mallInfo) {
        this.mallInfo = mallInfo;
    }
}
