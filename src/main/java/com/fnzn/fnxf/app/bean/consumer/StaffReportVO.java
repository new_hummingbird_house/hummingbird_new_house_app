package com.fnzn.fnxf.app.bean.consumer;

import com.fnzn.fnxf.app.entity.adviser.BuildingInfoAdviser;
import com.fnzn.fnxf.app.entity.building.BuildingAdviser;
import com.fnzn.fnxf.app.entity.consumer.StaffReport;

import java.util.List;

public class StaffReportVO extends StaffReport {
    private String buildingTitle;
    private String progressStatusText;
    private String adviserName;
    private List<BuildingAdviser> adviserList;

    public List<BuildingAdviser> getAdviserList() {
        return adviserList;
    }

    public void setAdviserList(List<BuildingAdviser> adviserList) {
        this.adviserList = adviserList;
    }

    public String getBuildingTitle() {
        return buildingTitle;
    }

    public void setBuildingTitle(String buildingTitle) {
        this.buildingTitle = buildingTitle;
    }

    public String getProgressStatusText() {
        return progressStatusText;
    }

    public void setProgressStatusText(String progressStatusText) {
        this.progressStatusText = progressStatusText;
    }

    public String getAdviserName() {
        return adviserName;
    }

    public void setAdviserName(String adviserName) {
        this.adviserName = adviserName;
    }
}
