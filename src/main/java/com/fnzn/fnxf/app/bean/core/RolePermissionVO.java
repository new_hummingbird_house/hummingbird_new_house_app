package com.fnzn.fnxf.app.bean.core;

import com.fnzn.fnxf.app.entity.customer.Customer;
import java.util.Set;

/**
 * 前后端分离权限管理
 */
public class RolePermissionVO {
    private Customer customer;
    private Set<String> roles;
    private Set<String> appPermissions;

    public RolePermissionVO(Customer customer, Set<String> roles, Set<String> appPermissions) {
        this.customer = customer;
        this.roles = roles;
        this.appPermissions = appPermissions;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Set<String> getAppPermissions() {
        return appPermissions;
    }

    public void setAppPermissions(Set<String> appPermissions) {
        this.appPermissions = appPermissions;
    }
}
