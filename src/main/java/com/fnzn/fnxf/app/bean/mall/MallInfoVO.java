package com.fnzn.fnxf.app.bean.mall;

import com.fnzn.fnxf.app.entity.mall.MallInfo;
import com.fnzn.fnxf.app.entity.mall.MallInfoDetail;
import com.fnzn.fnxf.app.entity.mall.MallInfoPics;

import java.util.List;

/**
 * @author Administrator
 */
public class MallInfoVO {
    private MallInfo mallInfo;
    private List<MallInfoDetail> mallInfoDetailList;

    private List<MallInfoPics> mallInfoPicsList;

    public MallInfoVO() {
    }

    public List<MallInfoPics> getMallInfoPicsList() {
        return mallInfoPicsList;
    }

    public void setMallInfoPicsList(List<MallInfoPics> mallInfoPicsList) {
        this.mallInfoPicsList = mallInfoPicsList;
    }

    public MallInfo getMallInfo() {
        return mallInfo;
    }

    public void setMallInfo(MallInfo mallInfo) {
        this.mallInfo = mallInfo;
    }

    public List<MallInfoDetail> getMallInfoDetailList() {
        return mallInfoDetailList;
    }

    public void setMallInfoDetailList(List<MallInfoDetail> mallInfoDetailList) {
        this.mallInfoDetailList = mallInfoDetailList;
    }
}
