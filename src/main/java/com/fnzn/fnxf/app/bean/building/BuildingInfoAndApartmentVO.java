package com.fnzn.fnxf.app.bean.building;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fnzn.fnxf.app.entity.building.BuildingInfo;

import java.util.List;

/**
 * 楼盘与特色中间表
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BuildingInfoAndApartmentVO extends BuildingInfo {

    private List<String> buildingAdvantageList;

    public List<String> getBuildingAdvantageList() {
        return buildingAdvantageList;
    }

    public void setBuildingAdvantageList(List<String> buildingAdvantageList) {
        this.buildingAdvantageList = buildingAdvantageList;
    }
}
